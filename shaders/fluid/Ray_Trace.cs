#version 450 core
layout(local_size_x = 8, local_size_y = 4) in;

layout(rgba32f, binding = 0) uniform readonly image2D in_front;
layout(rgba32f, binding = 1) uniform readonly image2D in_back;

layout(rgba32f, binding = 2) uniform writeonly image2D out_resolved;

layout(binding = 3) uniform sampler3D in_volume;
layout(binding = 4) uniform sampler3D in_temp;

layout(location = 0) uniform vec3 cam_pos;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

#include <Blackbody.inc>

mat4 bayer = mat4(vec4(0.0, 0.5, 2.0 / 16.0, 10.0 / 16.0),
				  vec4(12.0 / 16.0, 0.25, 14.0 / 16.0, 6.0 / 16.0),
				  vec4(3.0 / 16.0, 11.0 / 16.0, 1.0 / 16.0, 9.0 / 16.0),
				  vec4(15.0 / 16.0, 7.0 / 16.0, 13.0 / 16.0, 5.0 / 16.0));

vec4 trace(vec3 start, vec3 dir, vec3 step, int iters, ivec2 screen);

float sample_dither(vec3 pos)
{
	ivec3 dither_pos = ivec3(pos * 128);

	ivec3 dither = dither_pos % 4;

	return bayer[dither.x][dither.y];
}
vec4 sample_volume(vec3 pos)
{
	vec4 ret;

	float ht = 0.5 / 96.0;

	vec3 offsets[7] = {
		vec3(0.0, 0.0, 0.0),
		vec3(ht, 0.0, 0.0),
		vec3(0.0, ht, 0.0),
		vec3(0.0, 0.0, ht),
		vec3(-ht, 0.0, 0.0),
		vec3(0.0, -ht, 0.0),
		vec3(0.0, 0.0, -ht)
	};

	float density = 0.0;
	float t = 0.0;

	int samples = 1;
	for(int i = 0; i < samples; i++)
	{
		density += texture(in_volume, pos + offsets[i]).r;
		t += texture(in_temp, pos + offsets[i]).r;
	}

	t /= float(samples);
	density /= float(samples);

	ret = density.rrrr;

	t *= 10000.0;
	t = mix(0.0, 3000.0, t);
	ret.rgb += blackbody(t);
	return ret;
}




void main()
{
	vec2 pos = vec2(0.5) + vec2(gid.xy);
	ivec2 ipos = ivec2(pos);

	vec3 start = imageLoad(in_front, ipos).rgb;
	vec3 end = imageLoad(in_back, ipos).rgb;

	if(start == vec3(0.0) && end != start)
	{
		//start = cam_pos;
	}

	vec3 dir = normalize(end - start);
	vec4 dest = vec4(0.0);

	int iters = 32;
	float len = length(end - start);
	float step_size = len / float(iters);

	vec3 step = dir * step_size;

	if(start != end)
	{
		dest = trace(start, dir, step, iters, ipos);
		//dest.rgb = dest.rgb / (1.0 + dest.rgb);
		dest.rgb = pow(dest.rgb, vec3(1.0 / 2.2));
	}
	
		//dest = vec4(1.0);
	imageStore(out_resolved, ipos, dest);
}

vec4 trace(vec3 start, vec3 dir, vec3 step, int iters, ivec2 screen)
{
	vec4 dest = vec4(0.0);
	vec3 pos = start;
	ivec2 dither = screen % 4;

	float bayer_val = bayer[dither.x][dither.y];
	pos += step * bayer_val;
	float dist = 0.0;
	for(int i = 0; i < iters; i++)
	{
		vec4 src = sample_volume(pos);
		src.rgb *= src.a;
		dest += (1.0 - dest.a) * src;

		if(dest.a > 1.0)
		{
			dest.a = 1.0;
			break;
		}

		if(pos.x > 1.0 || pos.y > 1.0 || pos.z > 1.0)
		{
			break;
		}
		pos.xyz += step;
		dist += length(step);
	}
	return dest;
}