// Seen a few examples that precompute offsets for dealing with boundaries
// Should've tried this earlier, but doesn't seem to have changed results.
// Errors must be elsewhere?

ivec2 getBoundaryOffset(ivec2 pos)
{
	ivec2 offset = ivec2(0);
	float lt = texelFetchOffset(in_obstacles, pos, 0, ivec2(-1, -1)).r;
	float rt = texelFetchOffset(in_obstacles, pos, 0, ivec2(-1, 1)).r;
	float lb = texelFetchOffset(in_obstacles, pos, 0, ivec2(1, -1)).r;
	float rb = texelFetchOffset(in_obstacles, pos, 0, ivec2(1, 1)).r;

	if(lt > 0.0f)
	{
		offset = ivec2(1, 1);
	}

	if(rt > 0.0f)
	{
		offset = ivec2(-1, -1);
	}

	if(lb > 0.0f)
	{
		offset = ivec2(1, -1);
	}

	if(rb > 0.0f)
	{
		offset = ivec2(-1, -1);
	}

	if(offset.x != 0 && offset.y != 0)
	{
		return offset;
	}

	float l = texelFetchOffset(in_obstacles, pos, 0, ivec2(-1, 0)).r;
	float r = texelFetchOffset(in_obstacles, pos, 0, ivec2(1, 0)).r;
	float t = texelFetchOffset(in_obstacles, pos, 0, ivec2(0, -1)).r;
	float b = texelFetchOffset(in_obstacles, pos, 0, ivec2(0, 1)).r;

	

	if(l > 0.0f)
	{
		offset.x = 1;
	}

	if(r > 0.0f)
	{
		offset.x = -1;
	}

	if(t > 0.0f)
	{
		offset.y = 1;
	}

	if(b > 0.0f)
	{
		offset.y = -1;
	}

	

	return offset;
}

