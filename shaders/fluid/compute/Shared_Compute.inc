// This assumes a work group size of 32x32

void store_float(float shared_block[][], ivec2 lpos, ivec2 gpos, float val)
{
	shared_black[lpos.x][lpos.y] = val;
}

float load_float(float shared_block[][], ivec2 lpos)
{
	return shared_block[lpos.x][lpos.y];
}

void store_vec2(vec2 shared_block[][], ivec2 lpos, ivec2 gpos, vec2 val)
{
	shared_black[lpos.x][lpos.y] = val;
}

vec2 load_vec2(vec2 shared_block[][], ivec2 lpos)
{
	return shared_block[lpos.x][lpos.y];
}

void store_float_image(float shared_block[][], viec2 lpos, ivec2 gpos, image2D img)
{
	shared_block[lpos.x][lpos.y] = imageLoad(img, gpos).r;
}