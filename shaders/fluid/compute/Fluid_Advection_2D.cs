#version 450 core
layout(local_size_x = 32, local_size_y = 32) in;

layout(location = 0) uniform float inv_grid_size;
layout(location = 1) uniform float delta_t;
layout(location = 2) uniform float dissipation;

// scale of advected quantity vs everything else
layout(location = 3) uniform float quantity_scale;

layout(binding = 0) uniform sampler2D in_quantity;
layout(rg32f, binding = 1) uniform readonly image2D in_velocity;
layout(r32f, binding = 2) uniform readonly image2D in_obstacles;

layout(rg32f, binding = 0) uniform writeonly image2D out_quantity;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{
	vec2 pos = vec2(0.5) + vec2(gid.xy);
    vec2 uv = vec2(gid.xy / imageSize(out_quantity));
	vec2 scpos = vec2(gid.xy * quantity_scale);
    ivec2 ipos  = ivec2(pos);

    ivec2 spos = ivec2(vec2(0.5) + scpos);
	float blocked = imageLoad(in_obstacles, spos).r;
	if(blocked > 0)
	{
        imageStore(out_quantity, ipos, vec4(0.0));
		return;
	}

    vec2 v = imageLoad(in_velocity, spos).rg;
    v *= 1.0 / quantity_scale;
    vec2 uvs =  inv_grid_size * (pos - delta_t * v);

    vec4 val = texture(in_quantity, uvs) * dissipation;
    imageStore(out_quantity, ipos, val);
}