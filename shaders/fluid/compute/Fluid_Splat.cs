#version 450 core

layout(location = 0) uniform vec4 color;
layout(location = 1) uniform vec2 mouse;
layout(location = 2) uniform float inv_grid_size;
layout(location = 3) uniform float radius;

layout(location = 0) out vec2 out_color;
layout(location = 1) in vec2 in_uvs;

layout(binding = 0) uniform sampler2D in_source;
layout(binding = 2) uniform sampler2D in_obstacles;
void main()
{
	vec2 center = vec2(mouse);
	vec2 pt = (center - gl_FragCoord.xy);
	
	vec2 o = vec2(1.0f) - texture(in_obstacles, inv_grid_size * gl_FragCoord.xy).rr;
	
	float g = exp(-dot(pt, pt) / radius);
	out_color =  vec2(color.r, color.g) * o * exp(-dot(pt, pt) / radius);
	out_color += texelFetch(in_source, ivec2(gl_FragCoord.xy), 0).rg * (1.0f - g); 
}