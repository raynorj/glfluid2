#version 450 core
layout(local_size_x = 32, local_size_y = 32) in;



layout(rg32f, binding = 0) uniform writeonly image2D out_velocity;
layout(r32f, binding = 1) uniform readonly image2D in_temperature;
layout(rg32f, binding = 2) uniform readonly image2D in_velocity;
layout(r32f, binding = 3) uniform readonly image2D in_density;


uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{

	vec2 pos = vec2(0.5) + vec2(gid.xy);
    ivec2 ipos = ivec2(pos);

	float k = 1.0e-3;
	float sigma = 1.0e3;
    float d = imageLoad(in_density, ipos).r;
    float T0 = 0.0;
    float T = imageLoad(in_temperature, ipos).r;
    vec2 buoyancy = (-k * d + sigma * (T - T0)) * vec2(0.0, -1.0);

    vec2 vel = imageLoad(in_velocity, ipos).xy;

    vel += buoyancy;
    imageStore(out_velocity, ipos, vec4(vel.x, vel.y, 0.0, 0.0));
}