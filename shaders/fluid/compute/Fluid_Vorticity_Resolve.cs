#version 450 core
layout(local_size_x = 32, local_size_y = 32) in;

layout(location = 0) uniform float half_grid_size;
layout(location = 1) uniform vec2 vort_scale;
layout(location = 2) uniform float delta_t;

layout(rg32f, binding = 0) uniform writeonly image2D out_velocity;
layout(r32f, binding = 1) uniform readonly image2D in_vorticity;
layout(rg32f, binding = 2) uniform readonly image2D in_velocity;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{
	vec2 pos = vec2(0.5) + vec2(gid.xy);
    ivec2 ipos  = ivec2(pos);

	float lv = imageLoad(in_vorticity, ipos + ivec2(-1, 0)).r;
	float rv = imageLoad(in_vorticity, ipos + ivec2(1, 0)).r;
	float bv = imageLoad(in_vorticity, ipos + ivec2(0, -1)).r;
	float tv = imageLoad(in_vorticity, ipos + ivec2(0, 1)).r;
	float cv = imageLoad(in_vorticity, ipos).r;
  
	vec2 vforce = (half_grid_size) * vec2(abs(tv) - abs(bv), abs(rv) - abs(lv));
	//vforce = 0.5f * vec2(abs(tv) - abs(bv), abs(rv) - abs(lv));

	float eps = 2.4414e-8;
	//eps = 0.5f;

	float msqr = max(eps, dot(vforce, vforce));
	
	//vforce *= vforce * inversesqrt(msqr);
	vforce *= inversesqrt(msqr);
	vforce *=  vort_scale * cv * vec2(1.0f, -1.0f);

	vec2 nvel = imageLoad(in_velocity, ipos).rg + delta_t * vforce;
	imageStore(out_velocity, ipos, vec4(nvel.x, nvel.y, 0.0, 0.0));
}