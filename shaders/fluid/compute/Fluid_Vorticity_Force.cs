#version 450 core
layout(local_size_x = 32, local_size_y = 32) in;


layout(location = 0) uniform float half_grid_size;
layout(location = 1) uniform float inv_grid_size;

layout(rg32f, binding = 0) uniform writeonly image2D out_velocity;
layout(rg32f, binding = 1) uniform readonly image2D in_velocity;
layout(r32f, binding = 2) uniform readonly image2D in_obstacles;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{
	vec2 pos = vec2(0.5) + vec2(gid.xy);
    ivec2 ipos  = ivec2(pos);

	float blocked = imageLoad(in_obstacles, ipos).r;
	if(blocked > 0)
	{
		imageStore(out_velocity, ipos, vec4(0.0));
		return;
	}

	vec2 lv = imageLoad(in_velocity, ipos + ivec2(-1, 0)).rg;
	vec2 rv = imageLoad(in_velocity, ipos + ivec2(1, 0)).rg;
	vec2 bv = imageLoad(in_velocity, ipos + ivec2(0, -1)).rg;
	vec2 tv = imageLoad(in_velocity, ipos + ivec2(0, 1)).rg;
  
	float vorticity = (rv.y - lv.y) - (tv.x - bv.x);

	//vorticity *= inv_grid_size;
	vorticity *= 0.5;
	imageStore(out_velocity, ipos, vec4(vorticity, 0.0, 0.0, 0.0));
}