#version 450 core
layout(local_size_x = 32, local_size_y = 32) in;

layout(location = 0) uniform float inv_grid_size;
layout(location = 1) uniform float delta_t;
layout(location = 2) uniform float diff;
layout(location = 3) uniform float quantity_scale;

layout(rg32f, binding = 0) uniform writeonly image2D out_quantity;
layout(binding = 1) uniform sampler2D in_phi_n;
layout(binding = 2) uniform sampler2D in_phi_n_hat;
layout(binding = 3) uniform sampler2D in_phi_n_one_hat;
layout(rg32f, binding = 5) uniform image2D in_velocity;
layout(r32f, binding = 4) uniform readonly image2D in_obstacles;


uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{
	vec2 upos = vec2(0.5) + vec2(gid.xy);
    ivec2 ipos  = ivec2(upos);

    float blocked = imageLoad(in_obstacles, ipos).r;
	if(blocked > 0)
	{
		imageStore(out_quantity, ipos, vec4(0.0));
		return;
	}

    vec2 scpos = vec2(gid.xy * quantity_scale);
    ivec2 spos = ivec2(vec2(0.5) + scpos);

	vec2 v = imageLoad(in_velocity, ipos).rg;
	vec2 pos = (ipos - delta_t * v);
	
	ivec2 ppos = ivec2(pos);

	pos = floor(pos + vec2(0.5)) * inv_grid_size;

	vec2 phi_n_clamp[4];
	phi_n_clamp[0] = texelFetch(in_phi_n, ppos + ivec2(0, 0), 0).rg;
	phi_n_clamp[1] = texelFetch(in_phi_n, ppos + ivec2(1, 0), 0).rg;
	phi_n_clamp[2] = texelFetch(in_phi_n, ppos + ivec2(0, 1), 0).rg;
	phi_n_clamp[3] = texelFetch(in_phi_n, ppos + ivec2(1, 1), 0).rg;

	vec2 phi_min = min(min(phi_n_clamp[0], phi_n_clamp[1]), min(phi_n_clamp[2], phi_n_clamp[3]));
	vec2 phi_max = max(max(phi_n_clamp[0], phi_n_clamp[1]), max(phi_n_clamp[2], phi_n_clamp[3]));

	vec2 phi_n_one_hat = texture(in_phi_n_one_hat, pos).rg;
	vec2 phi_n_hat = texture(in_phi_n_hat, pos).rg;
	vec2 phi_n = texture(in_phi_n, pos).rg;

	vec2 phi_n_one = phi_n_one_hat + 0.5 * (phi_n - phi_n_hat);

	vec4 val = diff * vec4(max(min(phi_n_one, phi_max), phi_min), 0.0, 0.0);
	imageStore(out_quantity, ipos, val);
};