#version 450 core
#extension GL_NV_gpu_shader5 : enable
layout(local_size_x = 4, local_size_y = 4, local_size_z = 2) in;

layout(r32f, binding = 0) uniform writeonly image3D out_pressure;
layout(r32f, binding = 1) uniform readonly image3D in_pressure;
layout(r32f, binding = 2) uniform readonly image3D in_b;
layout(r32f, binding = 3) uniform readonly image3D in_obstacles;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;

void main()
{

	vec3 pos = vec3(0.5) + vec3(gid.xyz);
    ivec3 ipos = ivec3(pos);

    float cp = imageLoad(in_pressure, ipos).r;

    ivec3 lpos = ivec3(ipos.x - 1, ipos.y, ipos.z);
	ivec3 rpos = ivec3(ipos.x + 1, ipos.y, ipos.z);
	ivec3 bpos = ivec3(ipos.x, ipos.y - 1, ipos.z);
	ivec3 tpos = ivec3(ipos.x, ipos.y + 1, ipos.z);
	ivec3 dpos = ivec3(ipos.x, ipos.y, ipos.z - 1);
	ivec3 upos = ivec3(ipos.x, ipos.y, ipos.z + 1);
	


	float lp = imageLoad(in_pressure, lpos).r;
	float lo = imageLoad(in_obstacles, lpos).r;

	float rp = imageLoad(in_pressure, rpos).r;
	float ro = imageLoad(in_obstacles, rpos).r;

	float tp = imageLoad(in_pressure, tpos).r;
	float to = imageLoad(in_obstacles, tpos).r;

	float bp = imageLoad(in_pressure, bpos).r;
	float bo = imageLoad(in_obstacles, bpos).r;

	float up = imageLoad(in_pressure, upos).r;
	float uo = imageLoad(in_obstacles, upos).r;

	float dp = imageLoad(in_pressure, dpos).r;
	float dob = imageLoad(in_obstacles, dpos).r;


	lp = mix(lp, cp, lo);
	rp = mix(rp, cp, ro);
	bp = mix(bp, cp, bo);
	tp = mix(tp, cp, to);
	up = mix(up, cp, uo);
	dp = mix(dp, cp, dob);

	float cd = imageLoad(in_b, ipos).r;
	vec4 val = f16vec4((lp + rp + bp + tp + up + dp - cd) / 6.0, 0.0, 0.0, 0.0);

	imageStore(out_pressure, ipos, val);
}