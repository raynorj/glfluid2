#version 450 core
layout(local_size_x = 4, local_size_y = 4, local_size_z = 2) in;

layout(binding = 0) uniform sampler3D in_level;
layout(r32f, binding = 1) uniform writeonly image3D out_level;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

layout(location = 0) uniform int level;
layout(location = 1) uniform int scale;
layout(location = 2) uniform vec3 size;
void main()
{
	ivec3 dest_pos = ivec3(gid.xyz);
	ivec3 src_pos = dest_pos * ivec3(2);

	ivec3 pos[8] = {
			ivec3(1, 1, 1),
			ivec3(1, 1, 0),
			ivec3(1, 0, 1),
			ivec3(0, 1, 1),

			ivec3(1, 0, 0),
			ivec3(0, 1, 0),
			ivec3(0, 0, 1),
			ivec3(0, 0, 0)
	};

	vec4 mip = vec4(0.0);
	vec3 uv = vec3(dest_pos) / size;
	for(int i = 0; i < 8; i++)
	{
		mip += texelFetch(in_level, src_pos + pos[i], level);
		//mip += textureLod(in_level, uv, level);
	}

	mip /= 8.0;

	imageStore(out_level, dest_pos, mip);
}