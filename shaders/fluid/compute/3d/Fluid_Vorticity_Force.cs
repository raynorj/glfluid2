#version 450 core
layout(local_size_x = 4, local_size_y = 4, local_size_z = 2) in;


layout(location = 0) uniform float half_grid_size;
layout(location = 1) uniform float inv_grid_size;

layout(rgba32f, binding = 0) uniform writeonly image3D out_velocity;
layout(rgba32f, binding = 1) uniform readonly image3D in_velocity;
layout(r32f, binding = 2) uniform readonly image3D in_obstacles;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{
	vec3 pos = vec3(0.) + vec3(gid.xyz);
    ivec3 ipos  = ivec3(pos);

	float blocked = imageLoad(in_obstacles, ipos).r;
	if(blocked > 0)
	{
		imageStore(out_velocity, ipos, vec4(0.0));
		return;
	}

	ivec3 lpos = ivec3(ipos.x - 1, ipos.y, ipos.z);
	ivec3 rpos = ivec3(ipos.x + 1, ipos.y, ipos.z);
	ivec3 bpos = ivec3(ipos.x, ipos.y - 1, ipos.z);
	ivec3 tpos = ivec3(ipos.x, ipos.y + 1, ipos.z);
	ivec3 dpos = ivec3(ipos.x, ipos.y, ipos.z - 1);
	ivec3 upos = ivec3(ipos.x, ipos.y, ipos.z + 1);

	vec3 lv = imageLoad(in_velocity, lpos).xyz;
	vec3 rv = imageLoad(in_velocity, rpos).xyz;
	vec3 bv = imageLoad(in_velocity, bpos).xyz;
	vec3 tv = imageLoad(in_velocity, tpos).xyz;
	vec3 uv = imageLoad(in_velocity, upos).xyz;
	vec3 dv = imageLoad(in_velocity, dpos).xyz;
  
	float vorticity = (rv.y - lv.y) -
					  (tv.x - bv.x) -
					  (dv.z - uv.z);

	vorticity *= inv_grid_size;
	//vorticity *= 0.5;
	imageStore(out_velocity, ipos, vec4(vorticity, 0.0, 0.0, 0.0));
}