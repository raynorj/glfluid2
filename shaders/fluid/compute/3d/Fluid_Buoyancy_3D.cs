#version 450 core
#extension GL_NV_gpu_shader5 : enable
layout(local_size_x = 4, local_size_y = 4, local_size_z = 2) in;

layout(rgba32f, binding = 0) uniform writeonly image3D out_velocity;
layout(r32f, binding = 1) uniform readonly image3D in_temperature;
layout(rgba32f, binding = 2) uniform readonly image3D in_velocity;
layout(r32f, binding = 3) uniform readonly image3D in_density;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{

	vec3 pos = vec3(0.5) + vec3(gid.xyz);
    ivec3 ipos = ivec3(pos);

	float k = 1.0e-3;
	float sigma = 1.0e3;
    float d = imageLoad(in_density, ipos).r;
    float T0 = 0.0000;
    float T = imageLoad(in_temperature, ipos).r;
    vec3 buoyancy = vec3((-k * d + sigma * (T - T0)) * vec3(0.0, 1.0, 0.0));

    vec3 vel = vec3(imageLoad(in_velocity, ipos).xyz);

    //buoyancy = vec3(0.0, 1.0e-3, 0.0);
    vel += buoyancy;
    imageStore(out_velocity, ipos, vec4(vel, 0.0));
}