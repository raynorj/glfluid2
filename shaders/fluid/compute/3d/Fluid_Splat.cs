#version 450 core
layout(local_size_x = 4, local_size_y = 4, local_size_z = 2) in;

layout(rgba32f, binding = 1) uniform readonly image3D in_splat;
layout(rgba32f, binding = 0) uniform writeonly image3D out_splat;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;
void main()
{
	ivec3 pos = ivec3(gid.xyz);

	vec3 center = ivec3() / imageSize(out_splat);
	vec3 pt = (center - ivec3());
	
	vec2 o = vec2(1.0f) - texture(in_obstacles, inv_grid_size * gl_FragCoord.xy).rr;
	
	float g = exp(-dot(pt, pt) / radius);
	out_color =  o * exp(-dot(pt, pt) / radius);
	out_color += texelFetch(in_source, ivec2(gl_FragCoord.xy), 0).rg * (1.0f - g); 
}