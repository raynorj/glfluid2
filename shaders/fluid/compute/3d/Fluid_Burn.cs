#version 450 core
#extension GL_NV_gpu_shader5 : enable
layout(local_size_x = 4, local_size_y = 4, local_size_z = 2) in;

layout(location = 0) uniform float delta_t;

layout(r32f, binding = 0) uniform writeonly image3D out_fuel;
layout(r32f, binding = 1) uniform writeonly image3D out_density;
layout(r32f, binding = 2) uniform writeonly image3D out_temp;

layout(r32f, binding = 3) uniform readonly image3D in_fuel;
layout(r32f, binding = 4) uniform readonly image3D in_density;
layout(r32f, binding = 5) uniform readonly image3D in_temp;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{

	vec3 pos = vec3(0.5) + vec3(gid.xyz);
    ivec3 ipos = ivec3(pos);

    float fuel = imageLoad(in_fuel, ipos).r;
    float density = imageLoad(in_density, ipos).r;
    float t = imageLoad(in_temp, ipos).r;
    
    if(fuel > 0.0)
    {
        //fuel -=  delta_t * 0.005;
        t +=  delta_t * 0.00005;
        density += delta_t * 0.0125f;

    }
    imageStore(out_fuel, ipos, vec4(fuel, 0.0, 0.0, 0.0));
    imageStore(out_temp, ipos, vec4(t, 0.0, 0.0, 0.0));
    imageStore(out_density, ipos, vec4(density, 0.0, 0.0, 0.0));
}