#version 450 core
#extension GL_NV_gpu_shader5 : enable
layout(local_size_x = 4, local_size_y = 4, local_size_z = 2) in;

layout(location = 0) uniform float inv_grid_size;
layout(location = 1) uniform float delta_t;
layout(location = 2) uniform float dissipation;

// scale of advected quantity vs everything else
layout(location = 3) uniform float quantity_scale;

layout(binding = 0) uniform sampler3D in_quantity;
layout(rgba32f, binding = 1) uniform readonly image3D in_velocity;
layout(r32f, binding = 2) uniform readonly image3D in_obstacles;

layout(rgba32f, binding = 0) uniform writeonly image3D out_quantity;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{
	vec3 pos = vec3(0.5) + vec3(gid.xyz);
    vec3 uv = vec3(gid.xyz / imageSize(out_quantity));
	vec3 scpos = vec3(gid.xyz * quantity_scale);
    ivec3 ipos  = ivec3(pos);

    ivec3 spos = ivec3(vec3(0.5) + scpos);
	float blocked = imageLoad(in_obstacles, spos).r;
	if(blocked > 0)
	{
        imageStore(out_quantity, ipos, vec4(0.0));
		return;
	}

    vec3 v = imageLoad(in_velocity, spos).xyz;
    v *= 1.0 / quantity_scale;
    vec3 uvs = inv_grid_size * (pos - delta_t * v);

    vec4 val =  (texture(in_quantity, uvs) * dissipation);
    imageStore(out_quantity, ipos, val);
}