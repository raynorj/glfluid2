#version 450 core
#extension GL_NV_gpu_shader5 : enable
layout(local_size_x = 4, local_size_y = 4, local_size_z = 2) in;

layout(rgba32f, binding = 0) uniform writeonly image3D out_velocity;
layout(r32f, binding = 1) uniform readonly image3D in_pressure;
layout(rgba32f, binding = 2) uniform readonly image3D in_velocity;
layout(r32f, binding = 3) uniform readonly image3D in_obstacles;


uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{

	vec3 pos = vec3(0.5) + vec3(gid.xyz);
    ivec3 ipos = ivec3(pos);

	float blocked = imageLoad(in_obstacles, ipos).r;
	if(blocked > 0)
	{
		imageStore(out_velocity, ipos, vec4(0.0));
		return;
	}

    ivec3 lpos = ivec3(ipos.x - 1, ipos.y, ipos.z);
	ivec3 rpos = ivec3(ipos.x + 1, ipos.y, ipos.z);
	ivec3 bpos = ivec3(ipos.x, ipos.y - 1, ipos.z);
	ivec3 tpos = ivec3(ipos.x, ipos.y + 1, ipos.z);
	ivec3 dpos = ivec3(ipos.x, ipos.y, ipos.z - 1);
	ivec3 upos = ivec3(ipos.x, ipos.y, ipos.z + 1);

	/**///pressure 
	float lp = (imageLoad(in_pressure, lpos).r);
	float rp = (imageLoad(in_pressure, rpos).r);
	float bp = (imageLoad(in_pressure, bpos).r);
	float tp = (imageLoad(in_pressure, tpos).r);
	float up = (imageLoad(in_pressure, upos).r);
	float dp = (imageLoad(in_pressure, dpos).r);
	float cp = (imageLoad(in_pressure, ipos).r);

	//obstacles
	float lo = (imageLoad(in_obstacles, lpos).r);
	float ro = (imageLoad(in_obstacles, rpos).r);
	float bo = (imageLoad(in_obstacles, bpos).r);
	float to = (imageLoad(in_obstacles, tpos).r);
	float uo = (imageLoad(in_obstacles, upos).r);
	float dob = (imageLoad(in_obstacles, dpos).r);

	//handle boundaries
	
	vec3 vo = vec3(0.0);
    vec3 mask = vec3(1.0);

    lp = mix(lp, cp, lo);
	rp = mix(rp, cp, ro);
	bp = mix(bp, cp, bo);
	tp = mix(tp, cp, to);
	up = mix(up, cp, uo);
	dp = mix(dp, cp, dob);

	mask.x -= lo;
	mask.x -= ro;
	mask.y -= bo;
	mask.y -= to;
	mask.z -= uo;
	mask.z -= dob;

	mask = clamp(mask, vec3(0.0), vec3(1.0));

	vec3 vel = vec3(imageLoad(in_velocity, ipos).rgb);
    vec3 grad = vec3(rp - lp, tp - bp, up - dp) * (0.5);
    vec3 nvel = vel - grad;

    vec4 val = vec4(mask * nvel, 0.0);
	imageStore(out_velocity, ipos, val);
}