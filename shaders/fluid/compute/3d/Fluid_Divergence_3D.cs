#version 450 core
#extension GL_NV_gpu_shader5 : enable
layout(local_size_x = 4, local_size_y = 4, local_size_z = 2) in;

layout(r32f, location = 0) uniform writeonly image3D out_divergence;
layout(rgba32f, binding = 1) uniform readonly image3D in_velocity;
layout(r32f, binding = 2) uniform readonly image3D in_obstacles;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{
	vec3 pos = vec3(0.5) + vec3(gid.xyz);
    ivec3 ipos = ivec3(pos);
	

    ivec3 lpos = ivec3(ipos.x - 1, ipos.y, ipos.z);
	ivec3 rpos = ivec3(ipos.x + 1, ipos.y, ipos.z);
	ivec3 bpos = ivec3(ipos.x, ipos.y - 1, ipos.z);
	ivec3 tpos = ivec3(ipos.x, ipos.y + 1, ipos.z);
	ivec3 dpos = ivec3(ipos.x, ipos.y, ipos.z - 1);
	ivec3 upos = ivec3(ipos.x, ipos.y, ipos.z + 1);

	//velocity
	vec3 lv = imageLoad(in_velocity, lpos).xyz;
	vec3 rv = imageLoad(in_velocity, rpos).xyz;
	vec3 bv = imageLoad(in_velocity, bpos).xyz;
	vec3 tv = imageLoad(in_velocity, tpos).xyz;
	vec3 uv = imageLoad(in_velocity, upos).xyz;
	vec3 dv = imageLoad(in_velocity, dpos).xyz;

	//obstacles
	float lo = imageLoad(in_obstacles, lpos).r;
	float ro = imageLoad(in_obstacles, rpos).r;
	float bo = imageLoad(in_obstacles, bpos).r;
	float to = imageLoad(in_obstacles, tpos).r;
	float uo = imageLoad(in_obstacles, upos).r;
	float dob = imageLoad(in_obstacles, dpos).r;

	lv *= 1.0 - lo;
	rv *= 1.0 - ro;
	bv *= 1.0 - bo;
	tv *= 1.0 - to;
	uv *= 1.0 - uo;
	dv *= 1.0 - dob;

	f16vec4 val = f16vec4(0.5 * ((rv.x - lv.x) + (tv.y - bv.y) + (uv.z - dv.z)), 0.0, 0.0, 0.0);
	imageStore(out_divergence, ipos, val);
}