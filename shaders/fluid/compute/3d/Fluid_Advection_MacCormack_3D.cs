#version 450 core
layout(local_size_x = 4, local_size_y = 4, local_size_z = 2) in;

layout(location = 0) uniform float inv_grid_size;
layout(location = 1) uniform float delta_t;
layout(location = 2) uniform float diff;
layout(location = 3) uniform float quantity_scale;

layout(rgba32f, binding = 0) uniform writeonly image3D out_quantity;
layout(binding = 1) uniform sampler3D in_phi_n;
layout(binding = 2) uniform sampler3D in_phi_n_hat;
layout(binding = 3) uniform sampler3D in_phi_n_one_hat;
layout(r32f, binding = 4) uniform readonly image3D in_obstacles;

uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

void main()
{
	vec3 upos = vec3(0.5) + vec3(gid.xyz);
    ivec3 ipos  = ivec3(upos);
    vec3 scpos = vec3(gid.xyz * quantity_scale);


    ivec3 spos = ivec3(vec3(0.5) + scpos);

	vec3 v = texture(in_phi_n, ipos).xyz;
	vec3 pos = (upos - delta_t * v);
	vec3 half_texel = vec3(0.5 * inv_grid_size);

	upos *= inv_grid_size;
	pos *= inv_grid_size;

	//forward advection
	vec3 phi_n_one_hat = texture(in_phi_n_one_hat, pos).xyz;

	// fall back to first order if we're at a boundary
	float blocked = imageLoad(in_obstacles, spos).r;
	if(blocked > 0)
	{
		imageStore(out_quantity, ipos, vec4(phi_n_one_hat, 0.0));
		return;
	}
	//backward advection
	vec3 phi_n_hat = texture(in_phi_n_hat, pos).xyz;
	vec3 phi_n = texture(in_phi_n, upos).xyz;
	vec3 error = (0.5 * (phi_n - phi_n_hat));

	vec3 phi_n_one = phi_n_one_hat + error;

	vec3 phi_n_clamp[8];
	phi_n_clamp[0] = texture(in_phi_n, pos + vec3(half_texel.x, half_texel.y, half_texel.z)).xyz;
	phi_n_clamp[1] = texture(in_phi_n, pos + vec3(-half_texel.x, half_texel.y, half_texel.z)).xyz;
	phi_n_clamp[2] = texture(in_phi_n, pos + vec3(half_texel.x, -half_texel.y, half_texel.z)).xyz;
	phi_n_clamp[3] = texture(in_phi_n, pos + vec3(-half_texel.x, -half_texel.y, half_texel.z)).xyz;

	phi_n_clamp[4] = texture(in_phi_n, pos + vec3(half_texel.x, half_texel.y, -half_texel.z)).xyz;
	phi_n_clamp[5] = texture(in_phi_n, pos + vec3(-half_texel.x, half_texel.y, -half_texel.z)).xyz;
	phi_n_clamp[6] = texture(in_phi_n, pos + vec3(half_texel.x, -half_texel.y, -half_texel.z)).xyz;
	phi_n_clamp[7] = texture(in_phi_n, pos + vec3(-half_texel.x, -half_texel.y, -half_texel.z)).xyz;

	vec3 phi_min = min(min( min(phi_n_clamp[0], phi_n_clamp[1]), 
							min(phi_n_clamp[2], phi_n_clamp[3]) ),

					   min( min(phi_n_clamp[4], phi_n_clamp[5]), 
							min(phi_n_clamp[6], phi_n_clamp[7]) )
					);

	vec3 phi_max = max(max( max(phi_n_clamp[0], phi_n_clamp[1]), 
							max(phi_n_clamp[2], phi_n_clamp[3]) ),

					   max( max(phi_n_clamp[4], phi_n_clamp[5]), 
							max(phi_n_clamp[6], phi_n_clamp[7]) )
					);
	
	vec4 val = vec4(max(min(phi_n_one, phi_max), phi_min), 0.0);
	val = diff * vec4(phi_n_one_hat, 0.0);
	imageStore(out_quantity, ipos, val);
};