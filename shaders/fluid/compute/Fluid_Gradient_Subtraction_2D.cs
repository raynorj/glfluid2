#version 450 core
layout(local_size_x = 32, local_size_y = 32) in;

layout(rg32f, binding = 0) uniform writeonly image2D out_velocity;
layout(r32f, binding = 1) uniform readonly image2D in_pressure;
layout(rg32f, binding = 2) uniform readonly image2D in_velocity;
layout(r32f, binding = 3) uniform readonly image2D in_obstacles;


uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;

shared float shared_p[34][34];
shared float shared_o[34][34];

void load_shared(ivec2 lpos, ivec2 gpos, ivec2 offset)
{
	shared_p[lpos.x + offset.x][lpos.y + offset.y] = imageLoad(in_pressure, gpos + offset).r;
	shared_o[lpos.x + offset.x][lpos.y + offset.y] = imageLoad(in_obstacles, gpos + offset).r;
}


void main()
{

	vec2 pos = vec2(0.5) + vec2(gid.xy);
    ivec2 ipos = ivec2(pos);


	float blocked = imageLoad(in_obstacles, ipos).r;
	if(blocked > 0)
	{
		imageStore(out_velocity, ipos, vec4(0.0));
		return;
	}
	/**///pressure 
	float lp = imageLoad(in_pressure, ipos + ivec2(-1, 0)).r;
	float rp = imageLoad(in_pressure, ipos + ivec2(1, 0)).r;
	float bp = imageLoad(in_pressure, ipos + ivec2(0, -1)).r;
	float tp = imageLoad(in_pressure, ipos + ivec2(0, 1)).r;
	float cp = imageLoad(in_pressure, ipos).r;

	//obstacles
	float lo = imageLoad(in_obstacles, ipos + ivec2(-1, 0)).r;
	float ro = imageLoad(in_obstacles, ipos + ivec2(1, 0)).r;
	float bo = imageLoad(in_obstacles, ipos + ivec2(0, -1)).r;
	float to = imageLoad(in_obstacles, ipos + ivec2(0, 1)).r;//*/

	//handle boundaries
	
	vec2 vo = vec2(0.0);
    vec2 mask = vec2(1.0);

    lp = mix(lp, cp, lo);
	rp = mix(rp, cp, ro);
	bp = mix(bp, cp, bo);
	tp = mix(tp, cp, to);

	mask.x -= lo;
	mask.x -= ro;
	mask.y -= bo;
	mask.y -= to;

	mask = clamp(mask, vec2(0.0), vec2(1.0));

	vec2 vel = imageLoad(in_velocity, ipos).rg;
    vec2 grad = vec2(rp - lp, tp - bp) * (0.5);
    vec2 nvel = vel - grad;

    vec4 val = vec4(mask * nvel, 0.0, 0.0);
	imageStore(out_velocity, ipos, val);
}