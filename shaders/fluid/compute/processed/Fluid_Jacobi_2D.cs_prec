#version 450 core
layout(local_size_x = 32, local_size_y = 32) in;
layout(location = 0) uniform float alpha;
layout(location = 1) uniform float rec_beta;
layout(r32f, binding = 0) uniform writeonly image2D out_pressure;
layout(r32f, binding = 1) uniform readonly image2D in_pressure;
layout(r32f, binding = 2) uniform readonly image2D in_b;
layout(r32f, binding = 3) uniform readonly image2D in_obstacles;
uvec3 gid = gl_GlobalInvocationID;
uvec3 lid = gl_LocalInvocationID;
uvec3 group_size = gl_WorkGroupSize;
shared float shared_p[34][34];
shared float shared_o[34][34];
void load_shared(ivec2 lpos, ivec2 gpos, ivec2 offset)
{
	shared_p[lpos.x + offset.x][lpos.y + offset.y] = imageLoad(in_pressure, gpos + offset).r;
	shared_o[lpos.x + offset.x][lpos.y + offset.y] = imageLoad(in_obstacles, gpos + offset).r;
}
void main()
{
	vec2 pos = vec2(0.5) + vec2(gid.xy);
    ivec2 ipos = ivec2(pos);
    float cp = imageLoad(in_pressure, ipos).r;
    float co = imageLoad(in_obstacles, ipos).r;
    ivec2 cpos = ivec2(lid.x + 1, lid.y + 1);
    ivec2 lpos = ivec2(cpos.x - 1, cpos.y);
	ivec2 rpos = ivec2(cpos.x + 1, cpos.y);
	ivec2 tpos = ivec2(cpos.x, cpos.y - 1);
	ivec2 bpos = ivec2(cpos.x, cpos.y + 1);
    load_shared(cpos, ipos, ivec2(0, 0));
	// load left buffer
	if(lid.x == 0)
	{
		load_shared(cpos, ipos, ivec2(-1, 0));
	}
	// load right buffer
	if(lid.x == group_size.x - 1)
	{
		load_shared(cpos, ipos, ivec2(1, 0));
	}
	// load topbuffer
	if(lid.y == 0)
	{
		load_shared(cpos, ipos, ivec2(0, -1));
	}
	// load bottom buffer
	if(lid.y == group_size.y - 1)
	{
		load_shared(cpos, ipos, ivec2(0, 1));
	}
	memoryBarrierShared();
    barrier();
	float lp = shared_p[lpos.x][lpos.y];
	float lo = shared_o[lpos.x][lpos.y];
	float rp = shared_p[rpos.x][rpos.y];
	float ro = shared_o[rpos.x][rpos.y];
	float bp = shared_p[bpos.x][bpos.y];
	float bo = shared_o[bpos.x][bpos.y];
	
	float tp = shared_p[tpos.x][tpos.y];
	float to = shared_o[tpos.x][tpos.y];
	// With obstacles being binary, 
	// this should be faster because it removes branching
	lp = mix(lp, cp, lo);
	rp = mix(rp, cp, ro);
	bp = mix(bp, cp, bo);
	tp = mix(tp, cp, to);
	float cd = imageLoad(in_b, ipos).r;
	vec4 val = vec4((lp + rp + bp + tp - cd) * rec_beta, 0.0, 0.0, 0.0);
	imageStore(out_pressure, ipos, val);
}
