vec3 blackbody(float t)
{
	float t2 = t * t;

	// approximation of black body locus
	float u = (0.860117757 + 1.54118254e-4 * t + 1.28641212e-7 * t2) / (1.0 + 8.42420235e-4 * t + 7.08145163e-7 * t2); 
    float v = (0.317398726 + 4.22806245e-5 * t + 4.20481691e-8 * t2) / (1.0 - 2.89741816e-5 * t + 1.61456053e-7 * t2);

    // convert from CIE (above) to XYZ
    float uv = (2.0 * u - 8.0 * v + 4.0);
    float x = 3.0 * u / uv;
    float y = 2.0 * v / uv;
    float z = 1.0 - x - y;

    vec3 XYZ = vec3(1.0);
    XYZ.x = 1.0 / y * x;
    XYZ.z = 1.0 / y * z;

    // convert from XYZ to RGB
    mat3 toRGB = mat3(
    	3.2404542, -1.5371385, -0.4985314,
        -0.9692660, 1.8760108, 0.0415560,
        0.0556434, -0.2040259, 1.0572252);
    vec3 rgb = XYZ * toRGB;

    // weird approximation of Stefan–Boltzmann law
    return max(vec3(0.0), rgb * pow(4e-4 * t, 4.0));
}