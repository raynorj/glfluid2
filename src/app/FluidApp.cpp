#include "FluidApp.h"

void FluidApp::Init(string root_dir)
{
	GLApp::Init(root_dir);

	Debug = true;
	SinglePass = false;

	TwAddVarRW(m_UI, "Show Bounds", TW_TYPE_BOOL8, &Debug, "group=Visualization");
	TwAddVarRW(m_UI, "Single Pass", TW_TYPE_BOOL8, &SinglePass, "group=Visualization");

	const int GRID_SIZE = 64;
	const int voxels = GRID_SIZE * GRID_SIZE * GRID_SIZE;
	m_Volume = std::make_shared<Texture>();
	m_Volume->Create(TT_3D, TF_R16F, IF_R, TD_FLOAT, GRID_SIZE, GRID_SIZE, GRID_SIZE);

	float* data = new float[voxels];
	std::fill_n(data, voxels, 0.0f);

	// where position is (k, j , i)

	vec3 center(GRID_SIZE / 2);
	int radius = GRID_SIZE / 2;
	for (int i = 0; i < GRID_SIZE; i++)
	{
		for (int j = 0; j < GRID_SIZE; j++)
		{
			for (int k = 0; k < GRID_SIZE; k++)
			{
				float value = 0.0f;
				vec3 pos(k, j, i);
				//value = (glm::distance(center, pos) < radius) ? 0.5f : 0.0f;
				value = 1.0f - pow(glm::distance(center, pos) / radius, 2);
				data[(GRID_SIZE * GRID_SIZE * k) + (GRID_SIZE * j) + i] = value;
			}
		}
	}

	m_Volume->SetData(data);
	m_Renderer->Bind(2, m_Volume);
	m_Volume->SetWrapR(WM_BORDER);
	m_Volume->SetWrapS(WM_BORDER);
	m_Volume->SetWrapT(WM_BORDER);
	m_Volume->SetMinFilter(FM_LINEAR);
	m_Volume->SetMagFilter(FM_LINEAR);
	delete[] data;

	// use an external cube mesh for the volume right now
	m_Cube = m_Loader->LoadMesh("assets/cube/cube.obj");
	m_Cube->SetPosition(vec3(0.0f));
	m_Cube->SetRotation(vec3(0.0f));
	m_Cube->SetScale(vec3(2.4f));
	m_Scene = m_Cube;

	// basic volume setup
	m_VolumeFront = std::make_shared<Texture>();
	m_VolumeBack = std::make_shared<Texture>();
	m_VolumeFrontDepth = std::make_shared<Texture>();
	m_VolumeBackDepth = std::make_shared<Texture>();
	m_VolumeComposite = std::make_shared<Texture>();

	const int SCREEN_WIDTH = GetRenderer()->GetOptions().WindowWidth;
	const int SCREEN_HEIGHT = GetRenderer()->GetOptions().WindowHeight;

	m_VolumeFront->Create(TT_2D, TF_RGBA32F, IF_RGBA, TD_FLOAT, SCREEN_WIDTH, SCREEN_HEIGHT);
	m_VolumeBack->Create(TT_2D, TF_RGBA32F, IF_RGBA, TD_FLOAT, SCREEN_WIDTH, SCREEN_HEIGHT);
	m_VolumeComposite->Create(TT_2D, TF_RGBA32F, IF_RGBA, TD_FLOAT, SCREEN_WIDTH, SCREEN_HEIGHT);
	m_VolumeFrontDepth->Create(TT_2D, TF_DEPTH_32, IF_DEPTH, TD_FLOAT, SCREEN_WIDTH, SCREEN_HEIGHT);
	m_VolumeBackDepth->Create(TT_2D, TF_DEPTH_32, IF_DEPTH, TD_FLOAT, SCREEN_WIDTH, SCREEN_HEIGHT);

	vec3 black = vec3(0.0f);
	vec3 white = vec3(1.0f);
	GetRenderer()->Clear(m_VolumeFront, (void*)&black);
	GetRenderer()->Clear(m_VolumeBack, (void*)&black);
	GetRenderer()->Clear(m_VolumeComposite, (void*)&black);

	// shader setup
	m_ShellVS = GetLoader()->LoadShader("shaders/Ray_March_Shell.vs");
	m_ShellPS = GetLoader()->LoadShader("shaders/Ray_March_Shell.ps");
	m_RayDirDebug = m_Loader->LoadShader("shaders/Ray_Dir_Debug.ps");
	m_RayTraceTest = m_Loader->LoadShader("shaders/Ray_Trace_Two_Pass.ps");
	m_SinglePass = m_Loader->LoadShader("shaders/Ray_Trace_Triple_Pass.ps");
	m_BoundsView = m_Loader->LoadShader("shaders/Simple_Debug.ps");

	//fbo
	m_VolumeFBOFront = std::make_shared<Framebuffer>();
	m_VolumeFBOFront->Create(SCREEN_WIDTH, SCREEN_HEIGHT);
	m_VolumeFBOFront->BindTexture(0, m_VolumeFront);
	m_VolumeFBOFront->BindTexture(0, m_VolumeFrontDepth);
	m_VolumeFBOFront->Finish();

	m_VolumeFBOBack = std::make_shared<Framebuffer>();
	m_VolumeFBOBack->Create(SCREEN_WIDTH, SCREEN_HEIGHT);
	m_VolumeFBOBack->BindTexture(0, m_VolumeBack);
	m_VolumeFBOBack->BindTexture(0, m_VolumeBackDepth);
	m_VolumeFBOBack->Finish();


}

void FluidApp::Destroy()
{
	m_Volume->Destroy();
	GLApp::Destroy();
}

void FluidApp::Run()
{
	GLApp::Run();
}

void FluidApp::RenderFrame()
{
	//fluid sim steps
	//advection
	//diffusion
	//apply forces,
	//projection

	mat4 p = m_Camera->GetProjectionMatrix();
	mat4 v = m_Camera->GetViewMatrix();
	mat4 m = m_Cube->GetModelMatrix();
	mat4 mv = v * m;
	mat4 mvp = p * v * m;
	float focal_len = 1.0f / tan(m_Camera->GetFOV() / 2.0f);
	vec3 origin = transpose(mv) * vec4(m_Camera->GetPosition(), 1.0f);
	GLRenderer* r = GetRenderer();
	vec2 size(r->GetOptions().WindowWidth, r->GetOptions().WindowHeight);

	
	r->SetDepthWrite(true);
	r->Enable(OP_DEPTH_TEST);
	r->Enable(OP_CULL_FACE);
	//r-> Enable(OP_BLEND);
	r->SetPolygonMode(PF_FRONT_AND_BACK, PM_FILL);

	r->SetActiveShader(m_ShellVS);
	r->SetActiveShader(m_ShellPS);
	//r->SetBlendEquation(BF_ADD);
	// bounds front
	RenderVolumeFront();

	// bounds back
	RenderVolumeBack();
	//back
	m_RayTraceTest->SetUniform(3, mv);
	m_RayTraceTest->SetUniform(4, focal_len);
	m_RayTraceTest->SetUniform(5, size);
	m_RayTraceTest->SetUniform(6, origin);
	r->Bind(0, m_VolumeFront);
	r->Bind(1, m_VolumeBack);
	r->Bind(2, m_Volume);

	
	//quad, trace
	r->Enable(OP_BLEND);
	r->SetBlendFunc(BF_ONE, BF_ONE_MINUS_SRC_ALPHA);
	//r->SetBlendFunc(BF_ONE_MINUS_DST_ALPHA, BF_ONE);

	r->SetActiveShader(m_SinglePass);
	RayMarchTriplePass();
	r->Disable(OP_BLEND);
	r->Bind(0);

	// render wireframe bounds
	if (Debug)
	{
		r->Disable(OP_BLEND);
		r->SetPolygonMode(PF_FRONT_AND_BACK, PM_LINE);
		//r->SetDepthWrite(false);
		//r->Disable(OP_DEPTH_TEST);
		//r->Disable(OP_CULL_FACE);
		m_Renderer->SetCullFace(PF_BACK);
		r->SetActiveShader(m_ShellVS);
		r->SetActiveShader(m_BoundsView);

		for (int i = 0; i < m_Cube->GetPartCount(); i++)
		{
			VertexArray* vao = m_Cube->GetPart(i)->GetVertexArray();
			r->Bind(vao);
			r->DrawElements(vao->GetSize(), PT_TRI);
		}
		r->Bind(0);
		r->SetPolygonMode(PF_FRONT_AND_BACK, PM_FILL);
	}

}

void FluidApp::RenderVolumeFront()
{
	m_ShellVS->SetUniform(2, m_CameraMatrices.MVP);
	m_ShellVS->SetUniform(4, m_CameraMatrices.M);

	m_Renderer->SetCullFace(PF_BACK);
	m_VolumeFBOFront->Bind();
	m_VolumeFBOFront->BeginDraw();
	m_Renderer->Clear(BB_COLOR_DEPTH);
	for (int i = 0; i < m_Cube->GetPartCount(); i++)
	{
		VertexArray* vao = m_Cube->GetPart(i)->GetVertexArray();
		m_Renderer->Bind(vao);
		m_Renderer->DrawElements(vao->GetSize(), PT_TRI);
	}
	m_VolumeFBOFront->Unbind();
}

void FluidApp::RenderVolumeBack()
{
	m_ShellVS->SetUniform(2, m_CameraMatrices.MVP);
	m_ShellVS->SetUniform(4, m_CameraMatrices.M);

	m_Renderer->SetCullFace(PF_FRONT);
	m_VolumeFBOBack->Bind();
	m_VolumeFBOBack->BeginDraw();
	m_Renderer->Clear(BB_COLOR_DEPTH);

	for (int i = 0; i < m_Cube->GetPartCount(); i++)
	{
		VertexArray* vao = m_Cube->GetPart(i)->GetVertexArray();
		m_Renderer->Bind(vao);
		m_Renderer->DrawElements(vao->GetSize(), PT_TRI);
	}
	m_VolumeFBOFront->Unbind();
}

void FluidApp::RayMarchTriplePass()
{
#if 0
	m_Renderer->SetCullFace(PF_BACK);
	for (int i = 0; i < m_Cube->GetPartCount(); i++)
	{
		VertexArray* vao = m_Cube->GetPart(i)->GetVertexArray();
		m_Renderer->Bind(vao);
		m_Renderer->DrawElements(vao->GetSize(), PT_TRI);
	}
#endif // 0
	m_Renderer->RenderFSQuad();
}

void FluidApp::RayMarchSinglePass()
{
	m_Renderer->SetActiveShader(m_SinglePass);
	RenderVolumeFront();

}
