#include "FluidApp3D.h"

const f32 FluidApp3D::ONE_OVER_GRID_SIZE = 1.0f / (float)FluidApp3D::GRID_SIZE;
void FluidApp3D::Init(string root_dir)
{
	GLApp::Init(DEVELOPMENT_DIRECTORY);

	GLRenderer* r = GetRenderer();
	ResourceLoader* rl = GetLoader();

	TwAddVarRW(m_UI, "Pause", TW_TYPE_BOOLCPP, &m_Pause, "group=Renderer");
	TwAddVarRW(m_UI, "Enable Vorticity Confinement", TW_TYPE_BOOLCPP, &m_EnableVorticity, "group=Renderer");
	TwAddVarRW(m_UI, "Clear Simulation", TW_TYPE_BOOLCPP, &m_Clear, "group=Renderer");
	
	m_Pause = true;
	m_Step = false;
	m_Clear = false;
	m_EnableVorticity = false;
	m_MacCormackAdvection = false;

	m_Sim = new FluidSim3D(r, rl, ivec3(64));

	m_Cube = rl->LoadMesh("assets/cube/unit_cube.obj");

	m_Cube->SetPosition(vec3(0.0f));
	m_Cube->SetScale(vec3(35.0f));
	
	vec3 pos = vec3(100.0f, 10.0f, 100.0f);
	m_Camera->SetPosition(pos);
	m_Camera->SetDirection(normalize(vec3(0.0f) - pos));

	m_TraceVS = rl->LoadShader("shaders/fluid/Ray_March_Shell.vs");
	m_TracePS = rl->LoadShader("shaders/fluid/Ray_March_Shell.ps");
	m_TraceCS = rl->LoadShader("shaders/fluid/Ray_Trace.cs");
	m_DebugView = rl->LoadShader("shaders/Simple_Debug.ps");

	m_BlurCS = rl->LoadShader("shaders/fluid/Blur.cs");

	ivec2 size = r->GetOptions().WindowSize;

	m_Front = r->CreateRenderTarget2D(size / 2, TF_RGBA32F);
	m_Back = r->CreateRenderTarget2D(size / 2, TF_RGBA32F);
	m_VolumeTrace = r->CreateRenderTargetTexture(size / 2, TF_RGBA32F);
	m_TraceUpscale = r->CreateRenderTargetTexture(size, TF_RGBA32F);
	m_BlurH = r->CreateRenderTargetTexture(size / 2, TF_RGBA32F);
	m_BlurV = r->CreateRenderTargetTexture(size / 2, TF_RGBA32F);


	
}

void FluidApp3D::Destroy()
{
	GLApp::Destroy();
}

void FluidApp3D::Run()
{
	GLApp::Run();
}

void FluidApp3D::RenderFrame()
{

	GLRenderer* r = GetRenderer();
	InputHandler* i = GetInput();
	ivec2 mouse(i->GetMouseX(), i->GetMouseY());
	ivec2 size(r->GetOptions().WindowWidth, r->GetOptions().WindowHeight);

	float dt = GetDeltaTime() * 0.1f;
	//dt = 1.0f / 1.0f;
	if (m_Input->IsKeyJustPressed(SDLK_p))
	{
		m_Pause = !m_Pause;
	}

	if (m_Input->IsKeyJustReleased(SDLK_SPACE) && m_Pause)
	{
		m_Step = true;
	}


	//clear sim
	if (m_Clear)
	{
		m_Clear = false;
	}

	if (m_Step || !m_Pause)
	{
		m_Step = false;
		m_Sim->Update(dt);
	}

	m_Sim->SetVorticity(m_EnableVorticity);
	m_Sim->SetMacCormack(m_MacCormackAdvection);
	


	// render trace faces
	glViewport(0, 0, 620, 360);

	m_Renderer->SetActiveShader(m_TraceVS);
	m_Renderer->SetActiveShader(m_TracePS);

	m_Renderer->Enable(OP_CULL_FACE);
	m_Renderer->Enable(OP_DEPTH_TEST);
	m_Renderer->SetDepthWrite(true);
	mat4 v = m_Camera->GetViewMatrix();
	mat4 p = m_Camera->GetProjectionMatrix();

	mat4 mvp = p * v * m_Cube->GetModelMatrix();

	m_TraceVS->SetUniform(2, mvp);
	m_TraceVS->SetUniform(4, m_Cube->GetModelMatrix());

	m_Renderer->SetCullFace(PF_BACK);
	m_Front->GetFBO()->Clear(vec4(0.0f));
	m_Renderer->SetActiveFramebuffer(m_Front->GetFBO());
	for (int i = 0; i < m_Cube->GetPartCount(); i++)
	{
		VertexArray* vao = m_Cube->GetPart(i)->GetVertexArray();
		m_Renderer->Bind(vao);
		m_Renderer->DrawElements(vao->GetSize(), PT_TRI);
	}

	m_Renderer->SetCullFace(PF_FRONT);
	m_Back->GetFBO()->Clear(vec4(0.0f));
	m_Renderer->SetActiveFramebuffer(m_Back->GetFBO());
	for (int i = 0; i < m_Cube->GetPartCount(); i++)
	{
		VertexArray* vao = m_Cube->GetPart(i)->GetVertexArray();
		m_Renderer->Bind(vao);
		m_Renderer->DrawElements(vao->GetSize(), PT_TRI);
	}

	m_Renderer->SetActiveFramebuffer();
	glViewport(0, 0, 1280, 720);

	// ray trace
	m_Renderer->BindImage(0, m_Front->GetTexturePtr(), BA_READ);
	m_Renderer->BindImage(1, m_Back->GetTexturePtr(), BA_READ);
	m_Renderer->BindImage(2, m_VolumeTrace, BA_WRITE);

	m_Renderer->Bind(3, m_Sim->GetDensityTexture());
	m_Renderer->Bind(4, m_Sim->GetTemperatureTexture());

	m_Renderer->SetActiveShader(m_TraceCS);
	//m_TraceCS->SetUniform(0, vec3(pos.x, pos.y, pos.z));
	{
		rmt_ScopedOpenGLSample(RenderSim);
		m_TraceCS->Dispatch((size.x / 2) / 8, (size.y / 2) / 4, 1);
		glMemoryBarrier(MB_IMAGE);
	}
	{
		rmt_ScopedOpenGLSample(Blur);
		Blur(m_VolumeTrace);
		Blur(m_BlurV);
		Blur(m_BlurV);
	}
	

	// draw resolved
	//m_Renderer->Bind(0, m_VolumeTrace);
	m_Renderer->Bind(0, m_BlurV);
	m_Renderer->SetActiveShader(m_Renderer->GetQuadPS());
	m_Renderer->RenderFSQuad();
	
	// draw wireframe
	/*m_Renderer->SetCullFace(PF_BACK);

	m_Renderer->SetActiveShader(m_DebugView);
	m_Renderer->SetActiveShader(m_TraceVS);
	m_Renderer->SetPolygonMode(PF_FRONT_AND_BACK, PM_LINE);
	for (int i = 0; i < m_Cube->GetPartCount(); i++)
	{
		VertexArray* vao = m_Cube->GetPart(i)->GetVertexArray();
		m_Renderer->Bind(vao);
		m_Renderer->DrawElements(vao->GetSize(), PT_TRI);
	}

	m_Renderer->SetPolygonMode(PF_FRONT_AND_BACK, PM_FILL);*/
}

void FluidApp3D::Blur(TexturePtr input)
{
	ivec2 size = m_Renderer->GetOptions().WindowSize / 2;
	m_Renderer->SetActiveShader(m_BlurCS);

	m_BlurCS->SetUniform(0, vec2(1.0, 0.0));

	m_Renderer->BindImage(0, m_BlurH, BA_WRITE);
	m_Renderer->BindImage(1, input, BA_READ);
	m_BlurCS->Dispatch(size.x / 8, size.y / 4, 1);
	glMemoryBarrier(MB_IMAGE);

	m_BlurCS->SetUniform(0, vec2(0.0, 1.0));

	m_Renderer->BindImage(0, m_BlurV, BA_WRITE);
	m_Renderer->BindImage(1, m_BlurH, BA_READ);
	m_BlurCS->Dispatch(size.x / 8, size.y / 4, 1);
	glMemoryBarrier(MB_IMAGE);
}


