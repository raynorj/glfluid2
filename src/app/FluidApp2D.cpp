#include "FluidApp2D.h"

const f32 FluidApp2D::ONE_OVER_GRID_SIZE = 1.0f / (float)FluidApp2D::GRID_SIZE;
void FluidApp2D::Init(string root_dir)
{
	GLApp::Init(DEVELOPMENT_DIRECTORY);

	GLRenderer* r = GetRenderer();
	ResourceLoader* rl = GetLoader();

	r->SetWindowsSize(ivec2(512));

	m_DensityView = new Entity();
	//m_DensityView->SetPosition(vec3(-0.5f, 0.5f, 0.0f));
	//m_DensityView->SetScale(vec3(0.5f, 0.5f, 0.0f));
	m_DensityView->SetPosition(vec3(0.0f));
	m_DensityView->SetScale(vec3(1.0f));

	m_VelocityView = new Entity();
	m_VelocityView->SetPosition(vec3(0.5f, 0.5f, 0.0f));
	m_VelocityView->SetScale(vec3(0.5f, 0.5f, 0.0f));

	m_PressureView = new Entity();
	m_PressureView->SetPosition(vec3(-0.5f, -0.5f, 0.0f));
	m_PressureView->SetScale(vec3(0.5f, 0.5f, 0.0f));

	m_DivergenceView = new Entity();
	m_DivergenceView->SetPosition(vec3(0.5f, -0.5f, 0.0f));
	m_DivergenceView->SetScale(vec3(0.5f, 0.5f, 0.0f));


	m_SplatQuad = new Entity();
	m_SplatQuad->SetPosition(vec3(0.0f, 0.0f, 0.0f));
	m_SplatQuad->SetScale(vec3(0.4f, 0.4f, 0.0f));

	m_FSQuad = new Entity();
	m_FSQuad->SetPosition(vec3(0.0f));
	m_FSQuad->SetScale(vec3(1.0f));


	m_Velocity = CreatePingPongSurface(GRID_SIZE, GRID_SIZE, TF_RG32F);
	m_Density = CreatePingPongSurface(GRID_SIZE * DENSITY_SCALE, GRID_SIZE * DENSITY_SCALE, TF_R32F);
	m_Temperature = CreatePingPongSurface(GRID_SIZE * DENSITY_SCALE, GRID_SIZE * DENSITY_SCALE, TF_R32F);
	m_Pressure = CreatePingPongSurface(GRID_SIZE, GRID_SIZE, TF_R32F);
	m_DivergenceRT = CreateRenderTarget(GRID_SIZE, GRID_SIZE, TF_R32F);
	m_VorticityForceRT = CreateRenderTarget(GRID_SIZE, GRID_SIZE, TF_R32F);
	m_VorticityResolveRT = CreateRenderTarget(GRID_SIZE, GRID_SIZE, TF_R32F);

	m_MacCormackPhiNHat = CreateRenderTarget(GRID_SIZE, GRID_SIZE, TF_RG32F);
	m_MacCormackPhiNOneHat = CreateRenderTarget(GRID_SIZE, GRID_SIZE, TF_RG32F);

	m_MacCormackPhiNHatScaled = CreateRenderTarget(GRID_SIZE * DENSITY_SCALE, GRID_SIZE * DENSITY_SCALE, TF_RG32F);
	m_MacCormackPhiNOneHatScaled = CreateRenderTarget(GRID_SIZE * DENSITY_SCALE, GRID_SIZE * DENSITY_SCALE, TF_RG32F);

	m_DensityDownscale = CreateRenderTarget(GRID_SIZE, GRID_SIZE, TF_R32F);
	m_TemperatureDownscale = CreateRenderTarget(GRID_SIZE, GRID_SIZE, TF_R32F);

	m_Fuel = CreatePingPongSurface(GRID_SIZE * DENSITY_SCALE, GRID_SIZE * DENSITY_SCALE, TF_R32F);
	CreateWalls();
	SeedFluid();
	SeedVelocity();
	//SeedTemperature();

	std::vector<TexturePtr> tex_vec;
	tex_vec.push_back(m_Velocity->Ping.TextureHandle);
	tex_vec.push_back(m_Velocity->Pong.TextureHandle);

	tex_vec.push_back(m_Density->Ping.TextureHandle);
	tex_vec.push_back(m_Density->Pong.TextureHandle);
	tex_vec.push_back(m_DensityDownscale.TextureHandle);
	tex_vec.push_back(m_TemperatureDownscale.TextureHandle);

	tex_vec.push_back(m_Temperature->Ping.TextureHandle);
	tex_vec.push_back(m_Temperature->Pong.TextureHandle);

	tex_vec.push_back(m_Pressure->Ping.TextureHandle);
	tex_vec.push_back(m_Pressure->Pong.TextureHandle);


	tex_vec.push_back(m_MacCormackPhiNHat.TextureHandle);
	tex_vec.push_back(m_MacCormackPhiNOneHat.TextureHandle);

	tex_vec.push_back(m_DivergenceRT.TextureHandle);

	for (u32 i = 0; i < tex_vec.size(); i++)
	{
		TexturePtr t = tex_vec.at(i);

		t->SetWrapT(WM_CLAMP);
		t->SetWrapS(WM_CLAMP);
		t->SetMagFilter(FM_LINEAR);
		t->SetMinFilter(FM_LINEAR);
	}

	const int SCREEN_WIDTH = GetRenderer()->GetOptions().WindowWidth;
	const int SCREEN_HEIGHT = GetRenderer()->GetOptions().WindowHeight;

	m_MacCormackAdvect = rl->LoadShader("shaders/fluid/Fluid_Advection_MacCormack_2D.ps");

	m_DensityDebug = rl->LoadShader("shaders/fluid/Density_Debug.ps");
	m_VelocityDebug = rl->LoadShader("shaders/fluid/Velocity_Debug.ps");
	m_DivergenceDebug = rl->LoadShader("shaders/fluid/Divergence_Debug.ps");
	m_PressureDebug = rl->LoadShader("shaders/fluid/Pressure_Debug.ps");
	m_ObstacleDebug = rl->LoadShader("shaders/fluid/Obstacle_Offset_Debug.ps");

	m_FillShader = rl->LoadShader("shaders/fluid/Fill_Color.ps");
	m_FluidSplat = rl->LoadShader("shaders/fluid/Fluid_Splat.ps");
	m_ScreenQuadVS = rl->LoadShader("shaders/fluid/Movable_Screen_Quad.vs");
	m_FluidSeedShader = rl->LoadShader("shaders/fluid/Fluid_Seed_2D.ps");


	m_AdvectCS = rl->LoadShader("shaders/fluid/compute/Fluid_Advection_2D.cs");
	m_DivergenceCS = rl->LoadShader("shaders/fluid/compute/Fluid_Divergence_2D.cs");
	m_JacobiCS = rl->LoadShader("shaders/fluid/compute/Fluid_Jacobi_2D.cs");
	m_SubtractGradientCS = rl->LoadShader("shaders/fluid/compute/Fluid_Gradient_Subtraction_2D.cs");
	m_MacCormackAdvectCS = rl->LoadShader("shaders/fluid/compute/Fluid_Advection_MacCormack_2D.cs");
	m_VorticityForceCS = rl->LoadShader("shaders/fluid/compute/Fluid_Vorticity_Force.cs");
	m_VorticityResolveCS = rl->LoadShader("shaders/fluid/compute/Fluid_Vorticity_Resolve.cs");

	m_BuoyancyCS = rl->LoadShader("shaders/fluid/compute/Fluid_Buoyancy_2D.cs");
	m_BurnCS = rl->LoadShader("shaders/fluid/compute/Fluid_Burn.cs");

	m_QuadPS = rl->LoadShader("shaders/Full_Screen_Quad.ps");
	//fill initial walls
	{
		glViewport(0, 0, GRID_SIZE, GRID_SIZE);
		r->SetActiveShader(m_FluidSeedShader);
		r->SetActiveFramebuffer(m_Walls->Pong.FBOHandle);
		r->Bind(0, m_TempWalls);
		r->Bind(1, m_Walls->Ping.TextureHandle);
		r->RenderFSQuad();
	}


	//copy initial density into density texture
	/*{
		r->Enable(OP_BLEND);
		r->SetBlendEquation(BE_ADD);
		glViewport(0, 0, GRID_SIZE, GRID_SIZE);
		r->SetActiveShader(m_FluidSeedShader);
		r->SetActiveFramebuffer(m_Density->Pong.FBOHandle);
		r->Bind(0, m_FluidSeed);
		r->Bind(1, m_Density->Ping.TextureHandle);
		r->RenderFSQuad();
		r->Disable(OP_BLEND);
	}*/
	
	Splat(m_Fuel->Pong.FBOHandle, m_Fuel->Ping.TextureHandle, vec2(0.5f, 0.75f), 0.15f, vec4(1.0));
	
	
	//copy initial velocity into velocity texture
	bool use_static_velocity = true;
	if(use_static_velocity)
	{
		r->SetActiveShader(m_FillShader);
		r->SetActiveFramebuffer(m_Velocity->Pong.FBOHandle);
		r->Bind(2, m_Walls->Pong.TextureHandle);
		m_FillShader->SetUniform(0, vec4(0.0f, -GetScaledVelocity(vec2(0.05f)).y, 0.0f, 0.0f));
		r->RenderFSQuad();
	}
	else
	{
		r->Enable(OP_BLEND);
		r->SetBlendEquation(BE_ADD);
		glViewport(0, 0, GRID_SIZE, GRID_SIZE);
		r->SetActiveShader(m_FluidSeedShader);
		r->SetActiveFramebuffer(m_Velocity->Pong.FBOHandle);
		r->Bind(0, m_VelocitySeed);
		r->Bind(1, m_Velocity->Ping.TextureHandle);
		r->RenderFSQuad();
		r->Disable(OP_BLEND);
	}

	Fill(m_Temperature->Pong.FBOHandle, vec4(0.0f, 0.0f, 0.0f, 0.0f));
	r->SetActiveFramebuffer();

	TwAddVarRW(m_UI, "Pause", TW_TYPE_BOOLCPP, &m_Pause, "group=Renderer");
	TwAddVarRW(m_UI, "Enable Vorticity Confinement", TW_TYPE_BOOLCPP, &m_EnableVorticity, "group=Renderer");
	TwAddVarRW(m_UI, "Enable MacCormack Advection", TW_TYPE_BOOLCPP, &m_MacCormackAdvection, "group=Renderer");
	TwAddVarRW(m_UI, "Clear Simulation", TW_TYPE_BOOLCPP, &m_Clear, "group=Renderer");
	m_Pause = true;
	m_Step = false;
	m_Clear = false;
	m_EnableVorticity = false;
	m_MacCormackAdvection = false;
}

void FluidApp2D::Destroy()
{
	GLApp::Destroy();
}

void FluidApp2D::Run()
{
	GLApp::Run();
}

void FluidApp2D::RenderFrame()
{

	GLRenderer* r = GetRenderer();
	InputHandler* i = GetInput();
	ivec2 mouse(i->GetMouseX(), i->GetMouseY());
	ivec2 size(r->GetOptions().WindowWidth, r->GetOptions().WindowHeight);
	glViewport(0, 0, GRID_SIZE, GRID_SIZE);

	float dt = GetDeltaTime() * 0.01f;

	if (m_Input->IsKeyJustPressed(SDLK_p))
	{
		m_Pause = !m_Pause;
	}

	if (m_Input->IsKeyJustReleased(SDLK_SPACE) && m_Pause)
	{
		m_Step = true;
	}


	//clear sim
	if (m_Clear)
	{
		ClearSimulation();
		m_Clear = false;
	}

	if (m_Step || !m_Pause)
	{
	
		rmt_ScopedOpenGLSample(UpdateSimulation);
		/*
		advect needs 1 / grid_size
		and delta_t
		tex0 is thing to advect, tex1 is velocity
		wrap is clamp to edge
		render into ping, pong is source
		*/
		ivec2 gsize = ivec2(GRID_SIZE);
		ivec2 dsize = ivec2(GRID_SIZE * DENSITY_SCALE);
		//advect velocity
		{
			//if(m_MacCormackAdvection)
			{
				//AdvectMacCormackCS(m_Velocity->Ping.TextureHandle, m_Velocity->Pong.TextureHandle, gsize, dt, 0.999f);
				//AdvectMacCormack(m_Velocity->Ping.FBOHandle, m_Velocity->Pong.TextureHandle, dt, 0.99999f);
			}
			//else
			{
				rmt_ScopedOpenGLSample(AdvectVelocity);
				AdvectCS(m_Velocity->Ping.TextureHandle, m_Velocity->Pong.TextureHandle, gsize, dt, 0.999f);
			}
			SwapSurface(m_Velocity);
		}

		if (m_EnableVorticity)
		{
			rmt_ScopedOpenGLSample(Vorticity);
			VorticityForceCS();
			VorticityResolveCS(dt);
		}

		{
			rmt_ScopedOpenGLSample(Divergence);
			DivergenceCS();
		}


		{
			rmt_ScopedOpenGLSample(Pressure);
			JacobiCS(0, 0);
		}

		{
			rmt_ScopedOpenGLSample(GradientSub);
			GradientSubtractionCS();
		}

		//advect temperature
		{
			if (m_MacCormackAdvection)
			{
				AdvectMacCormackScaledCS(m_Temperature->Ping.TextureHandle, m_Temperature->Pong.TextureHandle, dsize, dt, 0.98f);
				//AdvectMacCormack(m_Temperature->Ping.FBOHandle, m_Temperature->Pong.TextureHandle, dt, 0.9999f);
			}
			else
			{
				rmt_ScopedOpenGLSample(AdvectTemp);
				AdvectCS(m_Temperature->Ping.TextureHandle, m_Temperature->Pong.TextureHandle, dsize, dt, 0.993f);
			}
			SwapSurface(m_Temperature);
		}

		//advect density
		{
			if (m_MacCormackAdvection)
			{
				AdvectMacCormackScaledCS(m_Density->Ping.TextureHandle, m_Density->Pong.TextureHandle, dsize, dt, 0.999f);
				//AdvectMacCormack(m_Density->Ping.FBOHandle, m_Density->Pong.TextureHandle, dt, 0.999f);
			}
			else
			{
				rmt_ScopedOpenGLSample(AdvectDensity);
				AdvectCS(m_Density->Ping.TextureHandle, m_Density->Pong.TextureHandle, dsize, dt, 0.999f);
			}
			SwapSurface(m_Density);
		}

		ivec2 pos = mouse;
		ivec2 win = ivec2(r->GetOptions().WindowWidth, r->GetOptions().WindowHeight);
		//pos %= win;
		vec2 rpos((float)pos.x / (float)win.x, ((float)pos.y / (float)win.y));
		//rpos *= 2.0f;
		m_SplatQuad->SetPosition(vec3(rpos.x - 1.0f, rpos.y - 1.0f, 0.0f));

		//apply impulses

		// downscale density since we're now using a higher res grid
		{
			m_Renderer->SetActiveFramebuffer(m_DensityDownscale.FBOHandle);
			m_Renderer->SetActiveShader(m_QuadPS);
			m_Renderer->Bind(0, m_Density->Pong.TextureHandle);
			m_Renderer->RenderFSQuad();
			m_Renderer->SetActiveFramebuffer();
		}


		// downscale temperature
		{
			m_Renderer->SetActiveFramebuffer(m_TemperatureDownscale.FBOHandle);
			m_Renderer->SetActiveShader(m_QuadPS);
			m_Renderer->Bind(0, m_Temperature->Pong.TextureHandle);
			m_Renderer->RenderFSQuad();
			m_Renderer->SetActiveFramebuffer();
		}


		// buoyancy
		{
			rmt_ScopedOpenGLSample(Buoyancy);
			ApplyBuoyancy();
		}


		// burn fuel for fire, emit smoke
		{
			rmt_ScopedOpenGLSample(Combustion);
			m_BurnCS->SetUniform(0, dt);
			ApplyBurn();
		}

		r->Bind(2, m_Walls->Pong.TextureHandle);

		// get velocity from mouse movement, then splat into velocity?
		if(m_Input->IsMouseButtonDown(MB_LEFT))
		{
			vec2 rel_movement(i->GetRelativeMouseX(), i->GetRelativeMouseY());
			float move_len = length(rel_movement);

			if (rel_movement.x != 0.0f && rel_movement.y != 0.0f)
			{
				rel_movement = normalize(rel_movement);
				rel_movement *= move_len * GetScaledVelocity(vec2(0.05f));

				r->Enable(OP_BLEND);
				r->SetBlendEquation(BE_ADD);
				Splat(m_Velocity->Ping.FBOHandle, m_Velocity->Pong.TextureHandle,
					rpos, 0.85f, vec4(rel_movement.x, rel_movement.y, 0.0f, 0.0f));
				SwapSurface(m_Velocity);
				r->Disable(OP_BLEND);	
			}
		}
		// splat density
		if(m_Input->IsMouseButtonDown(MB_RIGHT))
		{
			Splat(m_Density->Ping.FBOHandle, m_Density->Pong.TextureHandle, 
				rpos, 0.85f * (float)DENSITY_SCALE, vec4(1.0f, 0.0f, 0.0f, 0.0f));
			SwapSurface(m_Density);
		}

		// splat density from start
		/*{
			Splat(m_Density->Ping.FBOHandle, m_Density->Pong.TextureHandle, 
				vec2(0.5f, 0.85f), 0.85f * (float)DENSITY_SCALE, vec4(1.0f, 0.0f, 0.0f, 0.0f));
			SwapSurface(m_Density);
		}*/
		
		// splat walls
		if (m_Input->IsMouseButtonDown(MB_MIDDLE))
		{
			Splat(m_Walls->Ping.FBOHandle, m_Walls->Pong.TextureHandle,
			rpos, 0.005f, vec4(1.0f, 0.0f, 0.0f, 0.0f));
			SwapSurface(m_Walls);
		}
		

		// splat temperature
		if(m_Input->IsKeyPressed(SDLK_t))
		{
			//r->Enable(OP_BLEND);
			//r->SetBlendEquation(BE_ADD);

			Splat(m_Temperature->Ping.FBOHandle, m_Temperature->Pong.TextureHandle,
				rpos, 0.25f, vec4(0.1f * dt, 0.0f, 0.0f, 0.0f));
			//tr->Disable(OP_BLEND);
			SwapSurface(m_Temperature);
		}

		// splat fuel
		if (m_Input->IsKeyPressed(SDLK_f))
		{
			r->Enable(OP_BLEND);
			r->SetBlendEquation(BE_ADD);

			Splat(m_Fuel->Ping.FBOHandle, m_Fuel->Pong.TextureHandle,
				rpos, 0.15f, vec4(1.0f, 0.0f, 0.0f, 0.0f));
			r->Disable(OP_BLEND);
			SwapSurface(m_Fuel);
		}
		m_Step = false;
	}
	//show density
	glViewport(0, 0, size.x, size.y);
	r->SetActiveFramebuffer();
	DrawDebugView();

}

RenderTarget FluidApp2D::CreateRenderTarget(s32 width, s32 height, TextureFormat format, vec4 clear_color)
{
	RenderTarget ret;
	ret.TextureHandle = GetRenderer()->CreateRenderTargetTexture(ivec2(width, height), format, clear_color);
	std::vector<TexturePtr> vec;
	vec.push_back(ret.TextureHandle);

	ret.FBOHandle = GetRenderer()->CreateFramebuffer(ivec2(width, height), vec);
	return ret;
}

PingPongSurface* FluidApp2D::CreatePingPongSurface(s32 width, s32 height, TextureFormat format)
{
	PingPongSurface* surf = new PingPongSurface();
	surf->Ping = CreateRenderTarget(width, height, format);
	surf->Pong = CreateRenderTarget(width, height, format);
	return surf;
}

void FluidApp2D::SwapSurface(PingPongSurface* surface)
{
	RenderTarget temp = surface->Pong;
	surface->Pong = surface->Ping;
	surface->Ping = temp;
}

void FluidApp2D::CreateWalls()
{
	m_Walls = CreatePingPongSurface(GRID_SIZE, GRID_SIZE, TF_R32F);
	TexturePtr temp;
	m_TempWalls = std::make_shared<Texture>();
	m_TempWalls->Create(TT_2D, TF_R32F, IF_R, TD_FLOAT, GRID_SIZE, GRID_SIZE);

	u32 size = (GRID_SIZE) * (GRID_SIZE);
	f32* data = new f32[size];
	std::fill_n(data, size, 0.0f);

	for (int i = 0; i < GRID_SIZE; i++)
	{
		for (int j = 0; j < GRID_SIZE; j++)
		{
			u32 border_size = 3;
			//u32 value = 0;
			if (j < border_size || j > GRID_SIZE - border_size || i < border_size || i > GRID_SIZE - border_size)
			{
				data[(GRID_SIZE) * i + j] = 1.0f;
			}	

			if (glm::distance(vec2(i, j), vec2(GRID_SIZE / 2.0f, GRID_SIZE / 2.0f)) > GRID_SIZE / 2)
			{
				//data[(GRID_SIZE) * i + j] = 1.0f;
			}

			if (glm::distance(vec2(i, j), vec2(GRID_SIZE - (GRID_SIZE / 2.0f), GRID_SIZE / 2.0f)) < GRID_SIZE / 8)
			{
				//data[(GRID_SIZE) * i + j] = 1.0f;
			}
		}
	}
	m_TempWalls->SetData(data);
	delete[] data;
}

void FluidApp2D::SeedFluid()
{
	m_FluidSeed = std::make_shared<Texture>();
	m_FluidSeed->Create(TT_2D, TF_R32F, IF_R, TD_FLOAT, GRID_SIZE, GRID_SIZE);

	u32 size = (GRID_SIZE) * (GRID_SIZE);
	float* data = new float[size];
	std::fill_n(data, size, 0.0f);


	vec2 center((GRID_SIZE) / 2, (GRID_SIZE) / 4);
	int radius = GRID_SIZE / 6;
	for (int i = 0; i < GRID_SIZE; i++)
	{
		for (int j = 0; j < GRID_SIZE; j++)
		{
			vec2 pos(j, i);
			if(glm::distance(center, pos) < radius)
			{
				data[(GRID_SIZE) * i + j] = 1.0f;
			}
		}
	}
	m_FluidSeed->SetData(data);
	//m_Density->Pong.TextureHandle->SetData(data);
	delete[] data;
}

void FluidApp2D::SeedVelocity()
{
	m_VelocitySeed = std::make_shared<Texture>();
	m_VelocitySeed->Create(TT_2D, TF_RG32F, IF_RG, TD_FLOAT, GRID_SIZE, GRID_SIZE);

	u32 size = (GRID_SIZE) * (GRID_SIZE);
	vec2* data = new vec2[size];
	std::fill_n(data, size, vec2(0.0f));


	vec2 center((GRID_SIZE) / 2, (GRID_SIZE) / 2);
	int radius = GRID_SIZE / 4;
	for (int i = 0; i < GRID_SIZE; i++)
	{
		for (int j = 0; j < GRID_SIZE; j++)
		{
			
			vec2 pos(j, i);
			float dist = glm::distance(center, pos);
			float str = (dist) * 0.8f;
			str *= str * str * str;
			//if (dist > radius) str = 0.0f;
			vec2 vel_strength = -vec2(normalize(pos - center));
			//vel_strength *= str;
			vel_strength.y = 0.0f;

			vel_strength *= 0.1f;

			data[(GRID_SIZE) * i + j] = vel_strength;
		}
	}
	m_VelocitySeed->SetData(data);
	delete[] data;
}

void FluidApp2D::DrawDebugView()
{
	m_Renderer->SetQuadVS(m_ScreenQuadVS);
	//setup 
	m_Renderer->SetActiveFramebuffer();

	/**///velocity
	m_ScreenQuadVS->SetUniform(0, m_VelocityView->GetModelMatrix());
	m_Renderer->SetActiveShader(m_VelocityDebug);
	m_Renderer->Bind(0, m_Velocity->Pong.TextureHandle);
	//->RenderFSQuad();//*/


	//density
	m_ScreenQuadVS->SetUniform(0, m_DensityView->GetModelMatrix());
	m_Renderer->SetActiveShader(m_DensityDebug);
	m_Renderer->Bind(0, m_Density->Pong.TextureHandle);
	m_Renderer->Bind(1, m_Temperature->Pong.TextureHandle);
	m_Renderer->RenderFSQuad();

	/**/// fuel
	m_ScreenQuadVS->SetUniform(0, m_PressureView->GetModelMatrix());
	m_Renderer->SetActiveShader(m_PressureDebug);
	m_Renderer->Bind(0, m_Fuel->Pong.TextureHandle);
	//m_Renderer->RenderFSQuad();/**/

	

	/**/// temperature
	m_ScreenQuadVS->SetUniform(0, m_DivergenceView->GetModelMatrix());
	m_Renderer->SetActiveShader(m_PressureDebug);
	//m_Renderer->Bind(0, m_DivergenceRT.TextureHandle);
	//m_Renderer->Bind(0, m_VorticityForceRT.TextureHandle);
	m_Renderer->Bind(0, m_Temperature->Pong.TextureHandle);
	//m_Renderer->RenderFSQuad();

	m_Renderer->SetQuadVS(m_Loader->LoadShader("shaders/NDC_Passthrough.vs"));
}

void FluidApp2D::Splat(FramebufferPtr out_val, TexturePtr in_val, vec2 pos, f32 radius, vec4 color)
{
	glViewport(0, 0, in_val->GetWidth(), in_val->GetWidth());
	m_Renderer->SetActiveFramebuffer(out_val);

	vec2 inject = vec2(pos.x * in_val->GetWidth(), pos.y * in_val->GetHeight());
	m_FluidSplat->SetUniform(1, inject);
	m_FluidSplat->SetUniform(2, 1.0f / (float)in_val->GetWidth());
	m_FluidSplat->SetUniform(3, radius * in_val->GetWidth());

	m_ScreenQuadVS->SetUniform(0, m_SplatQuad->GetModelMatrix());

	m_Renderer->SetActiveShader(m_FluidSplat);
	m_Renderer->Bind(0, in_val);
	m_Renderer->SetActiveShader(m_ScreenQuadVS);
	m_FluidSplat->SetUniform(0, color);

	m_Renderer->RenderFSQuad();
}

void FluidApp2D::SplatFuel(FramebufferPtr out_val, TexturePtr in_val, vec2 pos, f32 radius, f32 amount, f32 ignition_temp, f32 out_heat)
{
}

void FluidApp2D::Fill(FramebufferPtr out_val, vec4 color)
{
	m_Renderer->SetActiveShader(m_FillShader);
	m_Renderer->SetActiveFramebuffer(out_val);
	m_Renderer->Bind(2, m_Walls->Pong.TextureHandle);
	m_FillShader->SetUniform(0, color);
	m_Renderer->RenderFSQuad();
}

void FluidApp2D::AdvectMacCormack(FramebufferPtr out_val, TexturePtr in_val, float dt, float diff)
{

	// intermediate advection to get phi_n_one_hat and phi_n_hat
	/*
		MacCormack advection is as follows
		phi_n = initial quantity

		phi_n_one_hat = Advect(phi_n)
		phi_n_hat = AdvectReverse(phi_n_one_hat)

		phi_n_one = phi_n_one_hat + (0.5 * (phi_n - phi_n_hat))

		After that, clamp to reasonable values to avoid blowing up

		See GPU Gems 3 ch 30 for more info

	*/
	//AdvectCS(m_MacCormackPhiNOneHat.TextureHandle, in_val, dt, diff);
	glMemoryBarrier(MB_IMAGE);
	//AdvectCS(m_MacCormackPhiNHat.TextureHandle, m_MacCormackPhiNOneHat.TextureHandle, -dt, 1.0 / diff);
	glMemoryBarrier(MB_IMAGE);

	m_Renderer->SetActiveShader(m_MacCormackAdvect);
	m_MacCormackAdvect->SetUniform(2, GetGridScale());
	m_MacCormackAdvect->SetUniform(3, dt);
	m_MacCormackAdvect->SetUniform(4, diff);
	m_Renderer->SetActiveFramebuffer(out_val);

	// phi_n is base quantity
	// phi_n_one_hat is forward advect
	// phi_n_hat is advected backward
	m_Renderer->Bind(0, in_val);
	m_Renderer->Bind(1, m_MacCormackPhiNHat.TextureHandle);
	m_Renderer->Bind(2, m_MacCormackPhiNOneHat.TextureHandle);
	m_Renderer->Bind(3, m_Walls->Pong.TextureHandle);
	m_Renderer->RenderFSQuad();
}

void FluidApp2D::AdvectCS(TexturePtr out_val, TexturePtr in_val, ivec2 size, float dt, float diff)
{
	m_Renderer->SetActiveShader(m_AdvectCS);
	m_AdvectCS->SetUniform(0, 1.0f / (float)size.x);
	m_AdvectCS->SetUniform(1, dt);
	m_AdvectCS->SetUniform(2, diff);
	m_AdvectCS->SetUniform(3, (float)m_Velocity->Pong.TextureHandle->GetWidth() / (float)size.x);

	m_Renderer->Bind(0, in_val);
	m_Renderer->BindImage(1, m_Velocity->Pong.TextureHandle, BA_READ);
	m_Renderer->BindImage(2, m_Walls->Pong.TextureHandle, BA_READ);
	m_Renderer->BindImage(0, out_val, BA_WRITE);

	m_AdvectCS->Dispatch(size.x / 32, size.y / 32, 1);
	glMemoryBarrier(MB_IMAGE);
}

void FluidApp2D::AdvectMacCormackCS(TexturePtr out_val, TexturePtr in_val, ivec2 size, float dt, float diff)
{
	AdvectCS(m_MacCormackPhiNOneHat.TextureHandle, in_val, size, dt, 1.0);
	glMemoryBarrier(MB_IMAGE);
	AdvectCS(m_MacCormackPhiNHat.TextureHandle, m_MacCormackPhiNOneHat.TextureHandle, size, -dt, 1.0);
	glMemoryBarrier(MB_IMAGE);
	m_Renderer->SetActiveShader(m_MacCormackAdvectCS);
	m_MacCormackAdvectCS->SetUniform(0, 1.0f / (float)size.x);
	m_MacCormackAdvectCS->SetUniform(1, dt);
	m_MacCormackAdvectCS->SetUniform(2, diff);
	m_MacCormackAdvectCS->SetUniform(3, (float)m_Velocity->Pong.TextureHandle->GetWidth() / (float)size.x);

	// phi_n is base quantity
	// phi_n_one_hat is forward advect
	// phi_n_hat is advected backward
	m_Renderer->BindImage(0, out_val, BA_WRITE);
	m_Renderer->Bind(1, in_val);
	m_Renderer->Bind(2, m_MacCormackPhiNHat.TextureHandle);
	m_Renderer->Bind(3, m_MacCormackPhiNOneHat.TextureHandle);
	m_Renderer->BindImage(5, m_Velocity->Pong.TextureHandle, BA_READ);
	m_Renderer->BindImage(4, m_Walls->Pong.TextureHandle, BA_READ);
	m_MacCormackAdvectCS->Dispatch(size.x / 32, size.y / 32, 1);
	glMemoryBarrier(MB_IMAGE);
}

void FluidApp2D::AdvectMacCormackScaledCS(TexturePtr out_val, TexturePtr in_val, ivec2 size, float dt, float diff)
{
	AdvectCS(m_MacCormackPhiNOneHatScaled.TextureHandle, in_val, size, dt, 1.0);
	glMemoryBarrier(MB_IMAGE);
	AdvectCS(m_MacCormackPhiNHatScaled.TextureHandle, m_MacCormackPhiNOneHatScaled.TextureHandle, size, -dt, 1.0);
	glMemoryBarrier(MB_IMAGE);
	m_Renderer->SetActiveShader(m_MacCormackAdvectCS);
	m_MacCormackAdvectCS->SetUniform(0, 1.0f / (float)size.x);
	m_MacCormackAdvectCS->SetUniform(1, dt);
	m_MacCormackAdvectCS->SetUniform(2, diff);
	m_MacCormackAdvectCS->SetUniform(3, (float)m_Velocity->Pong.TextureHandle->GetWidth() / (float)size.x);

	// phi_n is base quantity
	// phi_n_one_hat is forward advect
	// phi_n_hat is advected backward
	m_Renderer->BindImage(0, out_val, BA_WRITE);
	m_Renderer->Bind(1, in_val);
	m_Renderer->Bind(2, m_MacCormackPhiNHatScaled.TextureHandle);
	m_Renderer->Bind(3, m_MacCormackPhiNOneHatScaled.TextureHandle);
	m_Renderer->BindImage(4, m_Walls->Pong.TextureHandle, BA_READ);
	m_MacCormackAdvectCS->Dispatch(size.x / 32, size.y / 32, 1);
	glMemoryBarrier(MB_IMAGE);
}

void FluidApp2D::VorticityForceCS()
{
	m_Renderer->SetActiveShader(m_VorticityForceCS);
	m_VorticityForceCS->SetUniform(0, 0.5f * ONE_OVER_GRID_SIZE);
	m_VorticityForceCS->SetUniform(1, ONE_OVER_GRID_SIZE);
	m_Renderer->BindImage(0, m_VorticityForceRT.TextureHandle, BA_WRITE);
	m_Renderer->BindImage(1, m_Velocity->Pong.TextureHandle, BA_READ);
	m_Renderer->BindImage(2, m_Walls->Pong.TextureHandle, BA_READ);
	m_VorticityForceCS->Dispatch(GRID_SIZE / 32, GRID_SIZE / 32, 1);
	glMemoryBarrier(MB_IMAGE);
}

void FluidApp2D::VorticityResolveCS(float dt)
{
	m_Renderer->SetActiveShader(m_VorticityResolveCS);
	m_VorticityResolveCS->SetUniform(0, 0.5f * GetGridScale());
	m_VorticityResolveCS->SetUniform(1, vec2(GetGridScale() * 1000.0f));
	m_VorticityResolveCS->SetUniform(2, dt);

	m_Renderer->BindImage(0, m_Velocity->Ping.TextureHandle, BA_WRITE);
	m_Renderer->BindImage(1, m_VorticityForceRT.TextureHandle, BA_READ);
	m_Renderer->BindImage(2, m_Velocity->Pong.TextureHandle, BA_READ);
	m_VorticityResolveCS->Dispatch(GRID_SIZE / 32, GRID_SIZE / 32, 1);
	glMemoryBarrier(MB_IMAGE);
	SwapSurface(m_Velocity);
}

f32 FluidApp2D::GetGridScale()
{
	return 1.0f / (GRID_SIZE);
}

vec2 FluidApp2D::GetScaledVelocity(vec2 vel_sec)
{
	vec2 v = vel_sec;
	v *= ((float)GRID_SIZE);
	return v;
}

void FluidApp2D::ClearSimulation()
{
	m_Density->Pong.FBOHandle->Clear(vec4(0.0f));
	m_Velocity->Pong.FBOHandle->Clear(vec4(0.0f));
	m_Pressure->Pong.FBOHandle->Clear(vec4(0.0f));
	m_Density->Pong.FBOHandle->Clear(vec4(0.0f));
	m_Temperature->Pong.FBOHandle->Clear(vec4(0.0f));
	m_MacCormackPhiNHat.FBOHandle->Clear(vec4(0.0f));
	m_MacCormackPhiNOneHat.FBOHandle->Clear(vec4(0.0f));
	//m_Walls->Pong.FBOHandle->Clear(vec4(0.0f));
}

void FluidApp2D::DivergenceCS()
{
	m_Renderer->SetActiveShader(m_DivergenceCS);
	m_Renderer->BindImage(0, m_DivergenceRT.TextureHandle, BA_WRITE);
	m_Renderer->BindImage(1, m_Velocity->Pong.TextureHandle, BA_READ);
	m_Renderer->BindImage(2, m_Walls->Pong.TextureHandle, BA_READ);

	m_DivergenceCS->Dispatch(GRID_SIZE / 32, GRID_SIZE / 32, 1);
	glMemoryBarrier(MB_IMAGE);
}

void FluidApp2D::JacobiCS(float a, float b)
{
	m_Pressure->Pong.FBOHandle->BeginDraw();
	m_Pressure->Pong.FBOHandle->Clear(vec4(0.0f));

	float alpha = -GetGridScale() * GetGridScale();
	float rec_beta = 0.25f;

	m_Renderer->SetActiveShader(m_JacobiCS);
	m_JacobiCS->SetUniform(0, alpha);
	m_JacobiCS->SetUniform(1, rec_beta);
	m_Renderer->BindImage(2, m_DivergenceRT.TextureHandle, BA_READ);
	m_Renderer->BindImage(3, m_Walls->Pong.TextureHandle, BA_READ);

	for (u32 i = 0; i < 80; i++)
	{
		JacobiIterationCS();
	}
}

void FluidApp2D::JacobiIterationCS()
{
	m_Renderer->BindImage(0, m_Pressure->Ping.TextureHandle, BA_WRITE);
	m_Renderer->BindImage(1, m_Pressure->Pong.TextureHandle, BA_READ);
	m_JacobiCS->Dispatch(GRID_SIZE / 32, GRID_SIZE / 32, 1);
	glMemoryBarrier(MB_IMAGE);
	SwapSurface(m_Pressure);
}

void FluidApp2D::GradientSubtractionCS()
{
	m_Renderer->SetActiveShader(m_SubtractGradientCS);
	m_Renderer->BindImage(0, m_Velocity->Ping.TextureHandle, BA_WRITE);
	m_Renderer->BindImage(1, m_Pressure->Pong.TextureHandle, BA_READ);
	m_Renderer->BindImage(2, m_Velocity->Pong.TextureHandle, BA_READ);
	m_Renderer->BindImage(3, m_Walls->Pong.TextureHandle, BA_READ);

	m_SubtractGradientCS->Dispatch(GRID_SIZE / 32, GRID_SIZE / 32, 1);
	glMemoryBarrier(MB_IMAGE);
	SwapSurface(m_Velocity);
}

void FluidApp2D::ApplyBuoyancy()
{
	m_Renderer->SetActiveShader(m_BuoyancyCS);
	m_Renderer->BindImage(0, m_Velocity->Ping.TextureHandle, BA_WRITE);
	//m_Renderer->BindImage(1, m_Temperature->Pong.TextureHandle, BA_READ);
	m_Renderer->BindImage(1, m_TemperatureDownscale.TextureHandle, BA_READ);
	m_Renderer->BindImage(2, m_Velocity->Pong.TextureHandle, BA_READ);
	//m_Renderer->BindImage(3, m_Density->Pong.TextureHandle, BA_READ);
	m_Renderer->BindImage(3, m_DensityDownscale.TextureHandle, BA_READ);
	
	m_BuoyancyCS->Dispatch(GRID_SIZE / 32, GRID_SIZE / 32, 1);
	glMemoryBarrier(MB_IMAGE);
	SwapSurface(m_Velocity);
}

void FluidApp2D::ApplyBurn()
{
	m_Renderer->SetActiveShader(m_BurnCS);


	/*
	Burning subtracts from fuel, writing lower value out to fuel
	adds to density based on amount of fuel burned
	adds to temperature based on fuel heat
	but only if fuel is above burning point

	*/

	m_Renderer->BindImage(0, m_Fuel->Ping.TextureHandle, BA_WRITE);
	m_Renderer->BindImage(1, m_Density->Ping.TextureHandle, BA_WRITE);
	m_Renderer->BindImage(2, m_Temperature->Ping.TextureHandle, BA_WRITE);


	m_Renderer->BindImage(3, m_Fuel->Pong.TextureHandle, BA_READ);
	m_Renderer->BindImage(4, m_Density->Pong.TextureHandle, BA_READ);
	m_Renderer->BindImage(5, m_Temperature->Pong.TextureHandle, BA_READ);

	m_BurnCS->Dispatch((GRID_SIZE * DENSITY_SCALE) / 32, (GRID_SIZE * DENSITY_SCALE) / 32, 1);
	glMemoryBarrier(MB_IMAGE);
	SwapSurface(m_Fuel);
	SwapSurface(m_Density);
	SwapSurface(m_Temperature);
}
