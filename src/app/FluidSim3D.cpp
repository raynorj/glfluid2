#include "..\..\include\app\FluidSim3D.h"

FluidSim3D::FluidSim3D(GLRenderer* renderer, ResourceLoader* loader, ivec3 size)
{
	m_Renderer = renderer;
	m_GridSize = size;

	ResourceLoader* rl = loader;

	ivec3 grid = size;

	m_Velocity = CreatePingPongSurface(grid, TF_RGBA32F);
	m_Density = CreatePingPongSurface(grid * SCALE, TF_R32F, SCALE);
	m_Temperature = CreatePingPongSurface(grid * SCALE, TF_R32F, SCALE);
	m_Pressure = CreatePingPongSurface(grid, TF_R32F);

	m_DivergenceRT = renderer->CreateRenderTargetTexture(grid, TF_R32F);
	m_VorticityForceRT = renderer->CreateRenderTargetTexture(grid, TF_R32F);
	m_VorticityResolveRT = renderer->CreateRenderTargetTexture(grid, TF_R32F);

	//m_MacCormackPhiNHat = renderer->CreateRenderTargetTexture(grid, TF_RGBA32F);
	//m_MacCormackPhiNOneHat = renderer->CreateRenderTargetTexture(grid, TF_RGBA32F);

	//m_MacCormackPhiNHatScaled = renderer->CreateRenderTargetTexture(grid, TF_RGBA32F);
	//m_MacCormackPhiNOneHatScaled = renderer->CreateRenderTargetTexture(grid, TF_RGBA32F);

	m_DensityDownscale = renderer->CreateRenderTargetTexture(grid, TF_R32F);
	m_TemperatureDownscale = renderer->CreateRenderTargetTexture(grid, TF_R32F);

	m_Fuel = CreatePingPongSurface(grid * SCALE, TF_R32F);
	CreateWalls();

	SeedFuel();
	//SeedFluid();

	std::vector<TexturePtr> tex_vec;
	tex_vec.push_back(m_Velocity->Ping);
	tex_vec.push_back(m_Velocity->Pong);

	tex_vec.push_back(m_Density->Ping);
	tex_vec.push_back(m_Density->Pong);
	tex_vec.push_back(m_DensityDownscale);
	tex_vec.push_back(m_TemperatureDownscale);

	tex_vec.push_back(m_Temperature->Ping);
	tex_vec.push_back(m_Temperature->Pong);

	tex_vec.push_back(m_Pressure->Ping);
	tex_vec.push_back(m_Pressure->Pong);


	//tex_vec.push_back(m_MacCormackPhiNHat);
	//tex_vec.push_back(m_MacCormackPhiNOneHat);

	tex_vec.push_back(m_DivergenceRT);

	for (u32 i = 0; i < tex_vec.size(); i++)
	{
		TexturePtr t = tex_vec.at(i);

		t->SetWrapT(WM_CLAMP);
		t->SetWrapS(WM_CLAMP);
		t->SetWrapR(WM_CLAMP);
		t->SetMagFilter(FM_LINEAR);
		t->SetMinFilter(FM_LINEAR);
	}
	
	const int SCREEN_WIDTH = renderer->GetOptions().WindowWidth;
	const int SCREEN_HEIGHT = renderer->GetOptions().WindowHeight;

	m_FillShader = rl->LoadShader("shaders/fluid/Fill_Color.ps");

	m_AdvectCS = rl->LoadShader("shaders/fluid/compute/3d/Fluid_Advection_3D.cs");
	m_DivergenceCS = rl->LoadShader("shaders/fluid/compute/3d/Fluid_Divergence_3D.cs");
	m_JacobiCS = rl->LoadShader("shaders/fluid/compute/3d/Fluid_Jacobi_3D.cs");
	m_SubtractGradientCS = rl->LoadShader("shaders/fluid/compute/3d/Fluid_Gradient_Subtraction_3D.cs");
	m_MacCormackAdvectCS = rl->LoadShader("shaders/fluid/compute/3d/Fluid_Advection_MacCormack_3D.cs");
	m_VorticityForceCS = rl->LoadShader("shaders/fluid/compute/3d/Fluid_Vorticity_Force.cs");
	m_VorticityResolveCS = rl->LoadShader("shaders/fluid/compute/3d/Fluid_Vorticity_Resolve.cs");

	m_BuoyancyCS = rl->LoadShader("shaders/fluid/compute/3d/Fluid_Buoyancy_3D.cs");
	m_BurnCS = rl->LoadShader("shaders/fluid/compute/3d/Fluid_Burn.cs");
	m_Clear = rl->LoadShader("shaders/fluid/compute/3d/Fluid_Clear.cs");
	m_Downscale = rl->LoadShader("shaders/fluid/compute/3d/Fluid_Downscale.cs");
}

void FluidSim3D::Update(f32 dt)
{
	rmt_ScopedOpenGLSample(UpdateSimulation);

	ivec3 gsize = m_GridSize;
	ivec3 dsize = m_GridSize * SCALE;

	{
		//if (m_MacCormackAdvection)
		{
			//AdvectMacCormackCS(m_Velocity->Ping, m_Velocity->Pong, gsize, dt, 0.999f);
		}
		//else
		{
			rmt_ScopedOpenGLSample(AdvectVelocity);
			AdvectCS(m_Velocity->Ping, m_Velocity->Pong, gsize, dt, 0.999f);
		}
		SwapSurface(m_Velocity);
	}
	
	if (m_EnableVorticity)
	{
		rmt_ScopedOpenGLSample(Vorticity);
		VorticityForceCS();
		VorticityResolveCS(dt);
	}

	{
		rmt_ScopedOpenGLSample(Divergence);
		DivergenceCS();
	}
	
	
	{
		rmt_ScopedOpenGLSample(Pressure);
		JacobiCS(0, 0);
	}

	{
		rmt_ScopedOpenGLSample(GradientSub);
		GradientSubtractionCS();
	}
	

	//advect temperature
	//if(false)
	{
		if (m_MacCormackAdvection)
		{
			AdvectMacCormackScaledCS(m_Temperature->Ping, m_Temperature->Pong, dsize, dt, 0.993f);
		}
		else
		{
			rmt_ScopedOpenGLSample(AdvectTemp);
			AdvectCS(m_Temperature->Ping, m_Temperature->Pong, dsize, dt, 0.92f);
		}
		SwapSurface(m_Temperature);
	}

	//advect density
	//if(false)
	{
		if (m_MacCormackAdvection)
		{
			AdvectMacCormackScaledCS(m_Density->Ping, m_Density->Pong, dsize, dt, 0.999f);
		}
		else
		{
			rmt_ScopedOpenGLSample(AdvectDensity);
			AdvectCS(m_Density->Ping, m_Density->Pong, dsize, dt, 0.999f);
		}
		SwapSurface(m_Density);
	}

	//apply impulses

	// downscale density since we're now using a higher res grid
	/*{
	m_Renderer->SetActiveFramebuffer(m_DensityDownscale);
	m_Renderer->SetActiveShader(m_QuadPS);
	m_Renderer->Bind(0, m_Density->Pong);
	m_Renderer->RenderFSQuad();
	m_Renderer->SetActiveFramebuffer();
	}


	// downscale temperature
	{
	m_Renderer->SetActiveFramebuffer(m_TemperatureDownscale);
	m_Renderer->SetActiveShader(m_QuadPS);
	m_Renderer->Bind(0, m_Temperature->Pong);
	m_Renderer->RenderFSQuad();
	m_Renderer->SetActiveFramebuffer();
	}*/

	{
		rmt_ScopedOpenGLSample(Downscale);
		Downscale();
	}
	

	//bouyancy
	{
		rmt_ScopedOpenGLSample(Buoyancy);
		ApplyBuoyancy();
	}


	// burn fuel for fire, emit smoke
	
	{
		rmt_ScopedOpenGLSample(Combustion);
		ApplyBurn(dt);
	}
	
}

void FluidSim3D::SetVorticity(bool state)
{
	m_EnableVorticity = state;
}

void FluidSim3D::SetMacCormack(bool state)
{
	m_MacCormackAdvection = state;
}

TexturePtr FluidSim3D::GetDensityTexture()
{
	return m_Density->Pong;
}

TexturePtr FluidSim3D::GetTemperatureTexture()
{
	return m_Temperature->Pong;
}

TexturePtr FluidSim3D::GetVelocityTexture()
{
	return m_Velocity->Pong;
}


PingPongSurface* FluidSim3D::CreatePingPongSurface(ivec3 size, TextureFormat format, s32 levels)
{
	PingPongSurface* surf = new PingPongSurface();
	surf->Ping = m_Renderer->CreateRenderTargetTexture(size, format, levels);
	surf->Pong = m_Renderer->CreateRenderTargetTexture(size, format, levels);
	return surf;
}

void FluidSim3D::SwapSurface(PingPongSurface* surface)
{
	TexturePtr temp = surface->Pong;
	surface->Pong = surface->Ping;
	surface->Ping = temp;
}

void FluidSim3D::CreateWalls()
{

	m_Walls = std::make_shared<Texture>();
	m_Walls->Create(TT_3D, TF_R32F, IF_R, TD_FLOAT, m_GridSize.x, m_GridSize.y, m_GridSize.z);

	u32 size = m_GridSize.x * m_GridSize.y * m_GridSize.z;
	f32* data = new f32[size];
	std::fill_n(data, size, 0.0f);

	for (int i = 0; i < m_GridSize.x; i++)
	{
		for (int j = 0; j < m_GridSize.y; j++)
		{
			for (int k = 0; k <m_GridSize.z; k++)
			{
				s32 border_size = 3;
				s32 loc = i + m_GridSize.z * (j + m_GridSize.y * k);

				//u32 value = 0;
				if (j < border_size || j > m_GridSize.y - border_size ||
					i < border_size || i > m_GridSize.x - border_size ||
					k < border_size || k > m_GridSize.z - border_size)
				{
					
					data[loc] = 1.0f;
				}
				vec3 pos = vec3(i, j, k);
				vec3 center = vec3(m_GridSize.x / 2.0, m_GridSize.x - m_GridSize.y / 2.0, m_GridSize.z / 2.0);
				if (glm::distance(pos, center) < m_GridSize.x / 8)
				{
					//data[loc] = 1.0f;
				}
			}

		}
	}
	m_Walls->SetData(data);
	delete[] data;
}

void FluidSim3D::SeedFuel()
{
	ivec3 scaled = m_GridSize * SCALE;
	u32 size = scaled.x * scaled.y * scaled.z;
	f32* data = new f32[size];
	std::fill_n(data, size, 0.0f);

	for (int i = 0; i < scaled.x; i++)
	{
		for (int j = 0; j < scaled.y; j++)
		{
			for (int k = 0; k < scaled.z; k++)
			{
				u32 border_size = 3;

				vec3 pos = vec3(i, j, k);
				vec3 center = vec3(scaled.x / 2.0, scaled.y / 16.0, scaled.z / 2.0);
				s32 loc = i + scaled.z * (j + scaled.y * k);;

				if (glm::distance(pos, center) < scaled.x / 8)
				{
					data[loc] = 1.0f;
				}
			}

		}
	}
	m_Fuel->Pong->SetData(data);
	delete[] data;
}

void FluidSim3D::SeedFluid()
{
	u32 size = m_GridSize.x * m_GridSize.y * m_GridSize.z;
	f32* data = new f32[size];
	std::fill_n(data, size, 0.0f);

	for (int i = 0; i < m_GridSize.x; i++)
	{
		for (int j = 0; j < m_GridSize.y; j++)
		{
			for (int k = 0; k < m_GridSize.z; k++)
			{
				u32 border_size = 3;

				vec3 pos = vec3(i, j, k);
				vec3 center = vec3(m_GridSize.x / 2.0, m_GridSize.y / 4.0, m_GridSize.z / 2.0);
				s32 loc = i + m_GridSize.z * (j + m_GridSize.y * k);;
				//u32 value = 0;


				/*if (glm::distance(vec2(i, j), vec2(GRID_SIZE / 2.0f, GRID_SIZE / 2.0f)) > GRID_SIZE / 2)
				{
				//data[(GRID_SIZE) * i + j] = 1.0f;
				}*/

				if (glm::distance(pos, center) < m_GridSize.x / 2)
				{
					data[loc] = 0.4f;
				}
			}

		}
	}
	m_Density->Pong->SetData(data);
	delete[] data;
}

void FluidSim3D::SeedVelocity()
{
	u32 size = m_GridSize.x * m_GridSize.y * m_GridSize.z;
	vec3* data = new vec3[size];
	std::fill_n(data, size, vec3(0.0, 0.0, 0.0));

	for (int i = 0; i < m_GridSize.x; i++)
	{
		for (int j = 0; j < m_GridSize.y; j++)
		{
			for (int k = 0; k < m_GridSize.z; k++)
			{
				u32 border_size = 3;

				vec3 pos = vec3(i, j, k);
				vec3 center = vec3(m_GridSize.x / 2.0, m_GridSize.y / 4.0, m_GridSize.z / 2.0);
				s32 loc = i + m_GridSize.z * (j + m_GridSize.y * k);;
				//u32 value = 0;


				/*if (glm::distance(vec2(i, j), vec2(GRID_SIZE / 2.0f, GRID_SIZE / 2.0f)) > GRID_SIZE / 2)
				{
				//data[(GRID_SIZE) * i + j] = 1.0f;
				}*/

				if (glm::distance(pos, center) < m_GridSize.x / 2)
				{
					//data[loc] = 0.4f;
				}
			}

		}
	}
	m_Velocity->Pong->SetData(data);
	delete[] data;
}

void FluidSim3D::AdvectCS(TexturePtr out_val, TexturePtr in_val, ivec3 size, f32 dt, f32 diff)
{
	m_Renderer->SetActiveShader(m_AdvectCS);
	m_AdvectCS->SetUniform(0, 1.0f / (float)size.x);
	m_AdvectCS->SetUniform(1, dt);
	m_AdvectCS->SetUniform(2, diff);
	m_AdvectCS->SetUniform(3, (float)m_Velocity->Pong->GetWidth() / (float)size.x);

	m_Renderer->Bind(0, in_val);
	m_Renderer->BindImage(1, m_Velocity->Pong, BA_READ);
	m_Renderer->BindImage(2, m_Walls, BA_READ);

	m_Renderer->BindImage(0, out_val, BA_WRITE);

	m_AdvectCS->Dispatch(size.x / 4, size.y / 4, size.z / 2);
	glMemoryBarrier(MB_IMAGE);
}

void FluidSim3D::AdvectMacCormackCS(TexturePtr out_val, TexturePtr in_val, ivec3 size, f32 dt, f32 diff)
{
	AdvectCS(m_MacCormackPhiNOneHat, in_val, size, dt, 1.0);
	glMemoryBarrier(MB_IMAGE);
	AdvectCS(m_MacCormackPhiNHat, m_MacCormackPhiNOneHat, size, -dt, 1.0);
	glMemoryBarrier(MB_IMAGE);
	m_Renderer->SetActiveShader(m_MacCormackAdvectCS);
	m_MacCormackAdvectCS->SetUniform(0, 1.0f / (float)size.x);
	m_MacCormackAdvectCS->SetUniform(1, dt);
	m_MacCormackAdvectCS->SetUniform(2, diff);
	m_MacCormackAdvectCS->SetUniform(3, (float)m_Velocity->Pong->GetWidth() / (float)size.x);

	// phi_n is base quantity
	// phi_n_one_hat is forward advect
	// phi_n_hat is advected backward
	m_Renderer->BindImage(0, out_val, BA_WRITE);
	m_Renderer->Bind(1, in_val);
	m_Renderer->Bind(2, m_MacCormackPhiNHat);
	m_Renderer->Bind(3, m_MacCormackPhiNOneHat);
	m_Renderer->BindImage(4, m_Walls, BA_READ);
	m_MacCormackAdvectCS->Dispatch(size.x / 4, size.y / 4, size.z / 2);
	glMemoryBarrier(MB_IMAGE);
}

void FluidSim3D::AdvectMacCormackScaledCS(TexturePtr out_val, TexturePtr in_val, ivec3 size, f32 dt, f32 diff)
{
	AdvectCS(m_MacCormackPhiNOneHatScaled, in_val, size, dt, 1.0);
	glMemoryBarrier(MB_IMAGE);
	AdvectCS(m_MacCormackPhiNHatScaled, m_MacCormackPhiNOneHatScaled, size, -dt, 1.0);
	glMemoryBarrier(MB_IMAGE);
	m_Renderer->SetActiveShader(m_MacCormackAdvectCS);
	m_MacCormackAdvectCS->SetUniform(0, 1.0f / (float)size.x);
	m_MacCormackAdvectCS->SetUniform(1, dt);
	m_MacCormackAdvectCS->SetUniform(2, diff);
	m_MacCormackAdvectCS->SetUniform(3, (float)m_Velocity->Pong->GetWidth() / (float)size.x);

	// phi_n is base quantity
	// phi_n_one_hat is forward advect
	// phi_n_hat is advected backward
	m_Renderer->BindImage(0, out_val, BA_WRITE);
	m_Renderer->Bind(1, in_val);
	m_Renderer->Bind(2, m_MacCormackPhiNHatScaled);
	m_Renderer->Bind(3, m_MacCormackPhiNOneHatScaled);
	m_Renderer->BindImage(4, m_Walls, BA_READ);
	m_MacCormackAdvectCS->Dispatch(size.x / 4, size.y / 4, size.z / 2);
	glMemoryBarrier(MB_IMAGE);
}

void FluidSim3D::VorticityForceCS()
{
	m_Renderer->SetActiveShader(m_VorticityForceCS);
	m_VorticityForceCS->SetUniform(0, 0.5f * GetGridScale());
	m_VorticityForceCS->SetUniform(1, GetGridScale());
	m_Renderer->BindImage(0, m_VorticityForceRT, BA_WRITE);
	m_Renderer->BindImage(1, m_Velocity->Pong, BA_READ);
	m_Renderer->BindImage(2, m_Walls, BA_READ);
	m_VorticityForceCS->Dispatch(m_GridSize.x / 4, m_GridSize.y / 4, m_GridSize.z / 2);
	glMemoryBarrier(MB_IMAGE);
}

void FluidSim3D::VorticityResolveCS(f32 dt)
{
	m_Renderer->SetActiveShader(m_VorticityResolveCS);
	m_VorticityResolveCS->SetUniform(0, 0.5f * GetGridScale());
	m_VorticityResolveCS->SetUniform(1, vec3(GetGridScale() * 2000.0f));
	m_VorticityResolveCS->SetUniform(2, dt);

	m_Renderer->BindImage(0, m_Velocity->Ping, BA_WRITE);
	m_Renderer->BindImage(1, m_VorticityForceRT, BA_READ);
	m_Renderer->BindImage(2, m_Velocity->Pong, BA_READ);
	m_VorticityResolveCS->Dispatch(m_GridSize.x / 4, m_GridSize.y / 4, m_GridSize.z / 2);
	glMemoryBarrier(MB_IMAGE);
	SwapSurface(m_Velocity);
}

f32 FluidSim3D::GetGridScale()
{
	return 1.0f / (m_GridSize.x);
}

vec3 FluidSim3D::GetScaledVelocity(vec3 vel_sec)
{
	vec3 v = vel_sec;
	v *= ((vec3)m_GridSize);
	return v;
}

void FluidSim3D::ClearSimulation()
{
	/*m_Density->Pong.FBOHandle->Clear(vec4(0.0f));
	m_Velocity->Pong.FBOHandle->Clear(vec4(0.0f));
	m_Pressure->Pong.FBOHandle->Clear(vec4(0.0f));
	m_Density->Pong.FBOHandle->Clear(vec4(0.0f));
	m_Temperature->Pong.FBOHandle->Clear(vec4(0.0f));
	m_MacCormackPhiNHat.FBOHandle->Clear(vec4(0.0f));
	m_MacCormackPhiNOneHat.FBOHandle->Clear(vec4(0.0f));*/
	//m_Walls->Pong.FBOHandle->Clear(vec4(0.0f));
}

void FluidSim3D::DivergenceCS()
{
	m_Renderer->SetActiveShader(m_DivergenceCS);
	m_Renderer->BindImage(0, m_DivergenceRT, BA_WRITE);
	m_Renderer->BindImage(1, m_Velocity->Pong, BA_READ);
	m_Renderer->BindImage(2, m_Walls, BA_READ);

	m_DivergenceCS->Dispatch(m_GridSize.x / 4, m_GridSize.y / 4, m_GridSize.z / 2);
	glMemoryBarrier(MB_IMAGE);
}

void FluidSim3D::JacobiCS(f32 a, f32 b)
{
	//m_Pressure->Pong.FBOHandle->BeginDraw();
	//m_Pressure->Pong.FBOHandle->Clear(vec4(0.0f));
	Clear(m_Pressure->Pong);
	float alpha = -GetGridScale() * GetGridScale();
	float rec_beta = 0.25f;

	m_Renderer->SetActiveShader(m_JacobiCS);
	//m_JacobiCS->SetUniform(0, alpha);
	//m_JacobiCS->SetUniform(1, rec_beta);
	m_Renderer->BindImage(2, m_DivergenceRT, BA_READ);
	m_Renderer->BindImage(3, m_Walls, BA_READ);

	for (u32 i = 0; i < 120; i++)
	{
		JacobiIterationCS();
	}
}

void FluidSim3D::JacobiIterationCS()
{
	m_Renderer->BindImage(0, m_Pressure->Ping, BA_WRITE);
	m_Renderer->BindImage(1, m_Pressure->Pong, BA_READ);
	m_JacobiCS->Dispatch(m_GridSize.x / 4, m_GridSize.y / 4, m_GridSize.z / 2);
	glMemoryBarrier(MB_IMAGE);
	SwapSurface(m_Pressure);
}

void FluidSim3D::GradientSubtractionCS()
{
	m_Renderer->SetActiveShader(m_SubtractGradientCS);
	m_Renderer->BindImage(0, m_Velocity->Ping, BA_WRITE);
	m_Renderer->BindImage(1, m_Pressure->Pong, BA_READ);
	m_Renderer->BindImage(2, m_Velocity->Pong, BA_READ);
	m_Renderer->BindImage(3, m_Walls, BA_READ);

	m_SubtractGradientCS->Dispatch(m_GridSize.x / 4, m_GridSize.y / 4, m_GridSize.z / 2);
	glMemoryBarrier(MB_IMAGE);
	SwapSurface(m_Velocity);
}

void FluidSim3D::ApplyBuoyancy()
{
	m_Renderer->SetActiveShader(m_BuoyancyCS);
	m_Renderer->BindImage(0, m_Velocity->Ping, BA_WRITE);
	m_Renderer->BindImageLayer(1, m_Temperature->Pong, SCALE / 2, BA_READ);
	//m_Renderer->BindImage(1, m_TemperatureDownscale, BA_READ);
	m_Renderer->BindImage(2, m_Velocity->Pong, BA_READ);
	m_Renderer->BindImageLayer(3, m_Density->Pong, SCALE / 2, BA_READ);
	//m_Renderer->BindImage(3, m_DensityDownscale, BA_READ);

	m_BuoyancyCS->Dispatch(m_GridSize.x / 4, m_GridSize.y / 4, m_GridSize.z / 2);
	glMemoryBarrier(MB_IMAGE);
	SwapSurface(m_Velocity);
}

void FluidSim3D::ApplyBurn(f32 dt)
{
	m_BurnCS->SetUniform(0, dt);
	m_Renderer->SetActiveShader(m_BurnCS);

	m_Renderer->BindImage(0, m_Fuel->Ping, BA_WRITE);
	m_Renderer->BindImage(1, m_Density->Ping, BA_WRITE);
	m_Renderer->BindImage(2, m_Temperature->Ping, BA_WRITE);

	m_Renderer->BindImage(3, m_Fuel->Pong, BA_READ);
	m_Renderer->BindImage(4, m_Density->Pong, BA_READ);
	m_Renderer->BindImage(5, m_Temperature->Pong, BA_READ);

	m_BurnCS->Dispatch((m_GridSize.x  * SCALE) / 4, (m_GridSize.y * SCALE) / 4, (m_GridSize.z * SCALE) / 2);
	glMemoryBarrier(MB_IMAGE);
	SwapSurface(m_Fuel);
	SwapSurface(m_Density);
	SwapSurface(m_Temperature);
}

void FluidSim3D::Clear(TexturePtr texture)
{
	m_Renderer->BindImage(0, texture, BA_WRITE);
	m_Clear->Dispatch(texture->GetWidth() / 4, texture->GetHeight() / 4, texture->GetDepth() / 2);
	glMemoryBarrier(MB_IMAGE);
}

void FluidSim3D::Downscale()
{
	if (SCALE == 1)
		return;

	ivec3 dest_size = m_GridSize * SCALE / 2;
	m_Renderer->SetActiveShader(m_Downscale);

	for (int i = 0; i < SCALE - 1; i++)
	{
		m_Downscale->SetUniform(0, i);
		m_Downscale->SetUniform(1, SCALE);
		m_Downscale->SetUniform(2, vec3(dest_size));
		//density
		m_Renderer->Bind(0, m_Density->Pong);
		m_Renderer->BindImageLayer(1, m_Density->Pong, i+1, BA_WRITE);
		m_Downscale->Dispatch(dest_size.x / 4, dest_size.y / 4, dest_size.z / 2);
		//glMemoryBarrier(MB_IMAGE | MB_TEXTURE_FETCH);

		//temperature
		m_Renderer->Bind(0, m_Temperature->Pong);
		m_Renderer->BindImageLayer(1, m_Temperature->Pong, i+1, BA_WRITE);
		m_Downscale->Dispatch(dest_size.x / 4, dest_size.y / 4, dest_size.z / 2);

		glMemoryBarrier(MB_IMAGE | MB_TEXTURE_FETCH);
		dest_size /= 2;
	}
	
	

	//m_Density->Pong->GenerateMipMaps();
	//m_Temperature->Pong->GenerateMipMaps();
}
