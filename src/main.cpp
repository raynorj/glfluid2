#define GLM_FORCE_RADIANS
#include "FluidApp2D.h"
//#include "FluidApp3D.h"

using namespace std::chrono;

int main(int argc, char** argv)
{

	FluidApp2D* app = new FluidApp2D();
	//FluidApp3D* app = new FluidApp3D();
	app->Init(argv[0]);
	app->Run();
	app->Destroy();
	return 0;
}