#ifndef _FLUID_SIM_3D_H_
#define _FLUID_SIM_3D_H_
#include "GLApp.h"

class PingPongSurface
{
public:
	TexturePtr Ping = nullptr;
	TexturePtr Pong = nullptr;
};

class FluidSim3D
{
public:
	FluidSim3D(GLRenderer* renderer, ResourceLoader* loader, ivec3 size);
	~FluidSim3D();

	void Update(f32 dt);

	void SetVorticity(bool state);
	void SetMacCormack(bool state);

	TexturePtr GetDensityTexture();
	TexturePtr GetTemperatureTexture();
	TexturePtr GetVelocityTexture();

private:

	PingPongSurface* CreatePingPongSurface(ivec3 size, TextureFormat format, s32 level = 1);
	void SwapSurface(PingPongSurface* surface);

	void CreateWalls();
	void SeedFuel();
	void SeedFluid();
	void SeedVelocity();

	void AdvectCS(TexturePtr out_val, TexturePtr in_val, ivec3 size, f32 dt, f32 diff);
	void AdvectMacCormackCS(TexturePtr out_val, TexturePtr in_val, ivec3 size, f32 dt, f32 diff);
	void AdvectMacCormackScaledCS(TexturePtr out_val, TexturePtr in_val, ivec3 size, f32 dt, f32 diff);

	void VorticityForceCS();
	void VorticityResolveCS(f32 dt);
	void DivergenceCS();
	void JacobiCS(f32 a, f32 b);
	void JacobiIterationCS();

	void GradientSubtractionCS();
	void ApplyBuoyancy();
	void ApplyBurn(f32 dt);

	void Clear(TexturePtr texture);
	void Downscale();

	f32 GetGridScale();

	vec3 GetScaledVelocity(vec3 vel_sec);
	void ClearSimulation();
	
	bool m_EnableVorticity;
	bool m_MacCormackAdvection;

	GLRenderer* m_Renderer;
	ivec3 m_GridSize;

	PingPongSurface* m_Velocity;
	PingPongSurface* m_Density;
	PingPongSurface* m_Temperature;
	PingPongSurface* m_Pressure;
	PingPongSurface* m_Fuel;

	TexturePtr m_Walls;

	TexturePtr m_DivergenceRT;
	TexturePtr m_VorticityForceRT;
	TexturePtr m_VorticityResolveRT;
	TexturePtr m_MacCormackPhiNHat;
	TexturePtr m_MacCormackPhiNOneHat;

	TexturePtr m_MacCormackPhiNHatScaled;
	TexturePtr m_MacCormackPhiNOneHatScaled;

	TexturePtr m_DensityDownscale;
	TexturePtr m_TemperatureDownscale;

	ShaderPtr m_FillShader;
	ShaderPtr m_FluidSplat;

	ShaderPtr m_AdvectCS;
	ShaderPtr m_AdvectMacCormackCS;
	ShaderPtr m_DivergenceCS;
	ShaderPtr m_JacobiCS;
	ShaderPtr m_SubtractGradientCS;
	ShaderPtr m_MacCormackAdvectCS;
	ShaderPtr m_VorticityForceCS;
	ShaderPtr m_VorticityResolveCS;
	ShaderPtr m_BuoyancyCS;
	ShaderPtr m_BurnCS;

	ShaderPtr m_Clear;
	ShaderPtr m_Downscale;

	const int SCALE = 2;
};
#endif