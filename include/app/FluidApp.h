#include "GLApp.h"

class FluidApp : public GLApp
{
public:

	void Init(string root_dir) override;
	void Destroy() override;

	void Run() override;

	void RenderFrame() override;

	void RenderVolumeFront();
	void RenderVolumeBack();
	void RayMarchTriplePass();
	void RayMarchSinglePass();

private:
	TexturePtr m_Volume;
	TexturePtr m_VolumeFront;
	TexturePtr m_VolumeFrontDepth;
	TexturePtr m_VolumeBack;
	TexturePtr m_VolumeBackDepth;
	TexturePtr m_VolumeComposite;


	FramebufferPtr m_VolumeFBOFront;
	FramebufferPtr m_VolumeFBOBack;
	MeshPtr m_Cube;

	ShaderPtr m_ShellVS;
	ShaderPtr m_ShellPS;
	ShaderPtr m_RayDirDebug;
	ShaderPtr m_RayTraceTest;
	ShaderPtr m_BoundsView;
	ShaderPtr m_SinglePass;
	bool Debug;
	bool SinglePass;
};