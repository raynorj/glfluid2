#include "GLApp.h"

struct RenderTarget
{
	TexturePtr TextureHandle;
	FramebufferPtr FBOHandle;
};

struct PingPongSurface
{
	RenderTarget Ping;
	RenderTarget Pong;
};
class FluidApp2D : public GLApp
{
public:

	void Init(string root_dir) override;
	void Destroy() override;

	void Run() override;

	void RenderFrame() override;

	/*
		Probably should abstract this out at the GLRenderer level instead, 
		it will be useful for generic post processing as well
	*/
	RenderTarget CreateRenderTarget(s32 width, s32 height, TextureFormat format, vec4 clear_color = vec4(0.0f));
	PingPongSurface* CreatePingPongSurface(s32 width, s32 height, TextureFormat format);
	void SwapSurface(PingPongSurface* surface);

	/*
		Two ways to make obstacles, using a texture to represent them, and having a special shader for bounds
		I'm going to use a texture, as it seems simpler and far more flexible
		Obstacles are assumed to be stationary.
	*/
	void CreateWalls();
	void SeedFluid();
	void SeedVelocity();
	void DrawDebugView();


	/*
	Splitting these up into logical bits makes it so much neater
	*/
	void Splat(FramebufferPtr out_val, TexturePtr in_val, vec2 pos, f32 radius, vec4 color);
	void SplatFuel(FramebufferPtr out_val, TexturePtr in_val, vec2 pos, f32 radius, f32 amount, f32 ignition_temp, f32 out_heat);
	void Fill(FramebufferPtr out_val, vec4 color);

	void AdvectMacCormack(FramebufferPtr out_val, TexturePtr in_val, float dt, float diff);

	void AdvectCS(TexturePtr out_val, TexturePtr in_val, ivec2 size, float dt, float diff);
	void AdvectMacCormackCS(TexturePtr out_val, TexturePtr in_val, ivec2 size, float dt, float diff);
	void AdvectMacCormackScaledCS(TexturePtr out_val, TexturePtr in_val, ivec2 size, float dt, float diff);

	void VorticityForceCS();
	void VorticityResolveCS(float dt);
	void DivergenceCS();
	void JacobiCS(float a, float b);
	void JacobiIterationCS();

	void GradientSubtractionCS();
	void ApplyBuoyancy();
	void ApplyBurn();

	f32 GetGridScale();

	/*
		Scales a given velocity (input between 0 and 1) so that the velocity 
		travels at the same relative speed regardless of grid scale
	*/
	vec2 GetScaledVelocity(vec2 vel_sec);
	void ClearSimulation();

protected:
	PingPongSurface* m_Velocity;
	PingPongSurface* m_Density;
	PingPongSurface* m_Temperature;
	PingPongSurface* m_Pressure;
	PingPongSurface* m_Walls;

	
	TexturePtr m_FluidSeed;
	TexturePtr m_VelocitySeed;
	TexturePtr m_TempWalls;
	
	RenderTarget m_DivergenceRT;
	RenderTarget m_VorticityForceRT;
	RenderTarget m_VorticityResolveRT;
	RenderTarget m_MacCormackPhiNHat;
	RenderTarget m_MacCormackPhiNOneHat;

	RenderTarget m_MacCormackPhiNHatScaled;
	RenderTarget m_MacCormackPhiNOneHatScaled;

	RenderTarget m_DensityDownscale;

	RenderTarget m_TemperatureDownscale;

	PingPongSurface* m_Fuel;



	ShaderPtr m_MacCormackAdvect;
	ShaderPtr m_FluidSeedShader;
	ShaderPtr m_FillShader;
	ShaderPtr m_FluidSplat;

	ShaderPtr m_ObstacleDebug;
	ShaderPtr m_DensityDebug;
	ShaderPtr m_VelocityDebug;
	ShaderPtr m_PressureDebug;
	ShaderPtr m_DivergenceDebug;
	ShaderPtr m_ScreenQuadVS;

	ShaderPtr m_AdvectCS;
	ShaderPtr m_AdvectMacCormackCS;
	ShaderPtr m_DivergenceCS;
	ShaderPtr m_JacobiCS;
	ShaderPtr m_SubtractGradientCS;
	ShaderPtr m_MacCormackAdvectCS;
	ShaderPtr m_VorticityForceCS;
	ShaderPtr m_VorticityResolveCS;
	ShaderPtr m_BuoyancyCS;
	ShaderPtr m_BurnCS;
	ShaderPtr m_QuadPS;

	Entity* m_DensityView;
	Entity* m_VelocityView;
	Entity* m_PressureView;
	Entity* m_DivergenceView;
	Entity* m_SplatQuad;
	Entity* m_FSQuad;

	bool m_Step;
	bool m_Pause;
	bool m_Clear;
	bool m_EnableVorticity;
	bool m_MacCormackAdvection;

	static const s32 GRID_SIZE = 256;
	static const float ONE_OVER_GRID_SIZE;
	const u32 DENSITY_SCALE = 4;

};