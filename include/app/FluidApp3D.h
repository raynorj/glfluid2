#include "GLApp.h"
#include "FluidSim3D.h"

class FluidApp3D : public GLApp
{
public:

	void Init(string root_dir) override;
	void Destroy() override;

	void Run() override;

	void RenderFrame() override;

	void Blur(TexturePtr input);

protected:
	
	FluidSim3D* m_Sim;

	bool m_Step;
	bool m_Pause;
	bool m_Clear;
	bool m_EnableVorticity;
	bool m_MacCormackAdvection;

	static const s32 GRID_SIZE = 64;
	static const float ONE_OVER_GRID_SIZE;
	const u32 DENSITY_SCALE = 1;

	RenderTarget2D* m_Front;
	RenderTarget2D* m_Back;

	TexturePtr m_VolumeTrace;
	TexturePtr m_TraceUpscale;

	TexturePtr m_BlurH;
	TexturePtr m_BlurV;

	ShaderPtr m_TraceVS;
	ShaderPtr m_TracePS;
	ShaderPtr m_TraceCS;

	ShaderPtr m_BlurCS;

	ShaderPtr m_DebugView;

	MeshPtr m_Cube;

};